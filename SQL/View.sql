USE
Metro
GO

CREATE VIEW v_raspisanie 
AS 
SELECT start_vremya AS '�����������', s1.name AS '�� �������', finish_vremya AS '��������', s2.name AS '�� �������', vetka AS '�����'
	FROM (raspisanie inner join stanciya s1 on start_kod=kod) inner join stanciya s2 on finish_kod=s2.kod
GO


CREATE VIEW v_remont
AS
SELECT typik AS '�����', serial AS '������', name AS '�������������', start AS '����� ������', DATEADD(DAY,srok_dos_det+vremya_remonta,start) AS '����������������� ���������'
	FROM (remont inner join spravka on remont.kod=spravka.kod)
GO
