USE master
GO
CREATE DATABASE Metro
ON
(NAME='Metro_Data',
FILENAME='C:\Metro\Metro.mdf',
SIZE=5,
MAXSIZE=100,
FILEGROWTH=2)
LOG ON
(NAME='Metro_Log',
FILENAME='C:\Metro\Metro.ldf',
SIZE=5,	
MAXSIZE=100,
FILEGROWTH=2)
GO
USE Metro
GO

CREATE DEFAULT d_start_time
AS '5:15'
GO

CREATE DEFAULT d_finish_time
AS '23:55'
GO

CREATE RULE r_kvalif
AS @x IN ('������','������','������')
GO

CREATE TYPE t_doljnost
FROM nvarchar(20) not null
GO
CREATE DEFAULT d_doljnost
AS '���������'
GO
EXEC sp_bindefault 'd_doljnost', 't_doljnost'
GO
CREATE RULE r_doljnost
AS @x IN ('���������', '������� ���������')
GO
EXEC sp_bindrule 'r_doljnost', 't_doljnost'
GO

CREATE TYPE t_fio
FROM nvarchar(64) not null
GO
CREATE RULE  r_fio
AS
@x like '% %'
GO
exec sp_bindrule  'r_fio', 't_fio'
GO

CREATE TABLE typsostav (
	typkod int PRIMARY KEY,
	name nvarchar(20),
	kolmest int,
	dlina float CHECK (dlina < 200) not null,
	shirina int CHECK (shirina < 3000) not null,
	visota int CHECK (visota < 5000) not null,
	naznachenie nvarchar(30),
	koleya int CHECK (koleya=1520) not null,
	speed int
)

CREATE TABLE psostav (
	serial int PRIMARY KEY,
	typkod int,
	stat int,
	probeg int not null,
	iznos_motor int not null,
	iznos_tk int not null,
	constraint FK_typkod_psostav foreign key (typkod) references typsostav
)
GO

CREATE TABLE mashinist (
	serialpas float PRIMARY KEY, 
	serial int, 
	fio t_fio, 
	tel nvarchar(16) not null,
	staj int not null,
	kvalif nvarchar(10),
	constraint FK_serial_mashinist foreign key (serial) references psostav
)
GO
EXEC sp_bindrule 'r_kvalif', 'mashinist.kvalif'
GO

CREATE TABLE dispetcher (
	serialpas float PRIMARY KEY,
	fio t_fio,
	tel nvarchar(16) not null,
	doljnost t_doljnost,
	pas nvarchar(1000) not null
)
GO

CREATE TABLE stanciya (
	kod int PRIMARY KEY,
	name nvarchar(20),
	paspotok int,
	kod_vpered int,
	rast_vpered int,
	kod_nazad int,
	rast_nazad int,
	stat int,
	start_time time,
	finish_time time
)
GO

EXEC sp_bindefault 'd_start_time', 'stanciya.start_time'
EXEC sp_bindefault 'd_finish_time', 'stanciya.finish_time'


CREATE TABLE vetka (
	name nvarchar(20) PRIMARY KEY,
	start_stanciya int,
	finish_stanciya int,
	depo_status int,
	start_status int,
	finish_status int,
    constraint FK_start_stanciya foreign key (start_stanciya) references stanciya(kod),
    constraint FK_finish_stanciya foreign key (finish_stanciya) references stanciya(kod),
)
GO

CREATE TABLE spravka (
	kod int PRIMARY KEY,
	name nvarchar(30),
	krit float,
	srok_dos_det int,
	vremya_remonta int
)
GO

CREATE TABLE remont (
	typik int PRIMARY KEY,
	serial int,
	start datetime,
	kod int,
	constraint FK_kod_remont foreign key (kod) references spravka,
	constraint FK_serial_remont foreign key (serial) references psostav
)
GO

CREATE TABLE raspisanie (
	start_vremya time,
	start_kod int,
	finish_vremya time,
	finish_kod int,
	naprav int,
	vetka nvarchar(20),
	serial int,
	serialpas float,
	constraint PK_jurnal_raspisanie PRIMARY KEY (start_vremya,serialpas,serial,start_kod),
	constraint FK_serial_raspisanie foreign key (serial) references psostav,
	constraint FK_serialpas_raspisanie foreign key (serialpas) references dispetcher,
	constraint FK_start_kod_raspisanie foreign key (start_kod) references stanciya(kod),
	constraint FK_finish_kod_raspisanie foreign key (finish_kod) references stanciya(kod),
	constraint FK_vetka_raspisanie foreign key (vetka) references vetka(name)
)
GO

CREATE TABLE jurnal (
	start datetime,
	serialpas float,
	serial int,
	typik int,
	zametka nvarchar(30)
	constraint PK_jurnal PRIMARY KEY (start,serialpas,serial,typik),
	constraint FK_serial_jurnal foreign key (serial) references psostav,
	constraint FK_serialpas_jurnal foreign key (serialpas) references dispetcher,
	constraint FK_typik_jurnal foreign key (typik) references remont
)
GO