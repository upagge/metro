USE
Metro
GO

/* ���������� ������� � ����� */
CREATE FUNCTION kol_stanciy (@vetka nvarchar(20))
RETURNS INT AS
BEGIN
	DECLARE @kol int, @stanciya int
	SET @kol=1
	SELECT @stanciya = start_stanciya FROM vetka WHERE name=@vetka
	WHILE (SELECT kod_vpered FROM stanciya WHERE kod=@stanciya) IS NOT NULL
	BEGIN
		SET @kol = @kol + 1
		SET @stanciya=(SELECT kod_vpered FROM stanciya WHERE kod=@stanciya)
	END
	RETURN @kol
END
GO

CREATE FUNCTION adtime (@typ int, @stan int, @naprav int)
RETURNS INT AS
BEGIN
	DECLARE @s int
	IF (@naprav=0) 
		SELECT @s=rast_vpered FROM stanciya WHERE kod=@stan
	ELSE
		SELECT @s=rast_nazad FROM stanciya WHERE kod=@stan
	DECLARE @u int 
	SELECT @u=speed FROM typsostav WHERE typkod=@typ
	RETURN FLOOR(@s/@u)
END
GO

/* ���������� ������ ����� */
CREATE FUNCTION dlina_vetka (@vetka varchar(20))
RETURNS INT AS
BEGIN
	DECLARE @dlina int, @stanciya int
	SET @dlina=0
	SELECT @stanciya = start_stanciya FROM vetka WHERE name=@vetka
	WHILE (SELECT kod_vpered FROM stanciya WHERE kod=@stanciya) IS NOT NULL
	BEGIN
		SET @dlina = @dlina + (SELECT rast_vpered FROM stanciya WHERE kod=@stanciya)
		SET @stanciya=(SELECT kod_vpered FROM stanciya WHERE kod=@stanciya)
	END
	RETURN @dlina
END
GO

/* ���������� ���������� ����� ��������� */
CREATE FUNCTION dlina (@stanciya int, @finish int, @p int, @p2 int)
RETURNS INT AS
BEGIN
	DECLARE @next_stanciya int, @rastoyanie int
	SET @rastoyanie=0;
	WHILE 1=1
	BEGIN
		IF (@stanciya=@finish and @p=@p2) BREAK
		IF (@p=0)
		BEGIN
			SELECT @next_stanciya=kod_vpered FROM stanciya WHERE kod=@stanciya
			IF (@next_stanciya is NULL)
			BEGIN
				SELECT @next_stanciya=kod_nazad FROM stanciya WHERE kod=@stanciya
				SET @p=1
				IF (@stanciya=@finish and @p=@p2) BREAK
				SET @rastoyanie=@rastoyanie+(SELECT rast_nazad FROM stanciya WHERE kod=@stanciya)
			END
			ELSE SET @rastoyanie=@rastoyanie+(SELECT rast_vpered FROM stanciya WHERE kod=@stanciya)
		END
		ELSE
		BEGIN
			SELECT @next_stanciya=kod_nazad FROM stanciya WHERE kod=@stanciya
			IF (@next_stanciya is NULL)
			BEGIN
				SELECT @next_stanciya=kod_vpered FROM stanciya WHERE kod=@stanciya
				SET @p=0
				IF (@stanciya=@finish and @p=@p2) BREAK
				SET @rastoyanie=@rastoyanie+(SELECT rast_vpered FROM stanciya WHERE kod=@stanciya)
			END
			ELSE SET @rastoyanie=@rastoyanie+(SELECT rast_nazad FROM stanciya WHERE kod=@stanciya)
		END
		SET @stanciya=@next_stanciya
	END
	RETURN @rastoyanie
END
GO

/* �������������� �� ����� */
CREATE FUNCTION paspotok_vetki (@vetka nvarchar(20))
RETURNS INT AS
BEGIN
	DECLARE @paspotok int
	SET @paspotok=0;
	DECLARE @stanciya int
	SELECT @stanciya=start_stanciya FROM vetka WHERE name=@vetka
	WHILE @stanciya IS NOT NULL
	BEGIN
		IF (SELECT stat FROM stanciya WHERE kod=@stanciya) = 1
			SET @paspotok=@paspotok+(SELECT paspotok FROM stanciya WHERE kod=@stanciya)
		SET @stanciya=(SELECT kod_vpered FROM stanciya WHERE kod=@stanciya)
	END
	RETURN @paspotok 
END
GO

/* ����������� �������� ����� �������� */
CREATE FUNCTION start_interval(@vetka nvarchar(20), @typ int, @kol_sostav int)
RETURNS INT AS
BEGIN
	DECLARE @speed int, @interval int, @s int, @f int
	SELECT @s=start_stanciya, @f=finish_stanciya FROM vetka WHERE name=@vetka
	SELECT @speed=speed FROM typsostav WHERE typkod=@typ
	SET @interval = FLOOR((dbo.dlina_vetka(@vetka)*2)/@speed)+15*dbo.kol_stanciy(@vetka)*2
	RETURN CEILING(@interval/@kol_sostav)
END
GO

/*������������ ���������� �������� �� ����� */
CREATE FUNCTION maxi_sostav(@vetka nvarchar(20), @typ int)
RETURNS INT AS
BEGIN
	DECLARE @speed int, @interval int, @kol_sostav int, @s int, @f int
	SELECT @s=start_stanciya, @f=finish_stanciya FROM vetka WHERE name=@vetka
	SELECT @speed=speed FROM typsostav WHERE typkod=@typ
	SET @interval = FLOOR((dbo.dlina_vetka(@vetka)*2)/@speed)+15*dbo.kol_stanciy(@vetka)*2
	RETURN CEILING(@interval/90)
END
GO

/* �������� ������������ ��������� ������� */
CREATE FUNCTION proverka_sostav (@serial int)
RETURNS INT AS
BEGIN
	DECLARE @kod int
	SET @kod=0;
	IF (SELECT probeg FROM psostav WHERE serial=@serial) > (SELECT krit FROM spravka WHERE kod=2)
		SET @kod=2;
	ELSE IF (SELECT iznos_motor FROM psostav WHERE serial=@serial) > (SELECT krit FROM spravka WHERE kod=3)
			SET @kod=3;
		ELSE IF (SELECT iznos_tk FROM psostav WHERE serial=@serial) > (SELECT krit FROM spravka WHERE kod=1)
			SET @kod=1;
	RETURN @kod
END
GO

/* ����������� ���������� ������� */
CREATE FUNCTION sled_sostav (@ser int, @time time)
RETURNS INT AS
BEGIN
	DECLARE @vetka nvarchar(20), @nap int, @stanciya int
	SELECT  @stanciya=start_kod, @vetka=vetka, @nap=naprav FROM raspisanie WHERE serial=@ser and start_vremya<DATEADD(SECOND,15,@time) and finish_vremya>DATEADD(SECOND,-15,@time)
	DECLARE @temp int;
	SELECT TOP 1 @temp=serial
				FROM raspisanie
				WHERE start_vremya<DATEADD(SECOND,15,@time) and finish_vremya>DATEADD(SECOND,-15,@time) and serial!=@ser
				ORDER BY dbo.dlina(@stanciya,start_kod,@nap,naprav) ASC
	RETURN @temp
END
GO

/*
PRINT dbo.sled_sostav (20, '19:43')
GO
*/
