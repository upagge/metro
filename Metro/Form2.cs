﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security;
using System.Security.Cryptography;


namespace Metro
{
    public partial class Authorization : Form
    {
        const string connectionString = "Data Source=(local);" + "Initial Catalog=Metro;" + "User ID=sa;" + "Password=1950;";

        public Authorization()
        {
            InitializeComponent();
        }
        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (login.Text != "" && password.Text != "")
            {
                MD5 md5Hash = MD5.Create();
                string sql = "SELECT doljnost FROM dispetcher WHERE serialpas = " + login.Text + " and pas = 0x" + GetMd5Hash(md5Hash, password.Text);
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand cp = new SqlCommand(sql, connection);
                    get_doljnost.Value = (string)cp.ExecuteScalar();
                    SqlDataReader reader = cp.ExecuteReader();
                    if (reader.HasRows)
                    {
                        get_login.Value = float.Parse(login.Text);
                        Form a = new Form1();
                        a.Show();
                        this.Hide();
                    } else
                    {
                        MessageBox.Show("Неправильный логин или пароль");
                    }
                } 

            } else
            {
                MessageBox.Show("Введите данные для авторизации!");
            }
        }

        private void login_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }
    }
}
