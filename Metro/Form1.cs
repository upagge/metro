﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Metro
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void raspisanieBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.raspisanieBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.metroDataSet);

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Вы действительно хотите выйти из программы?","Завершение программы",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            if (dialog == DialogResult.Yes) { e.Cancel = false; Environment.Exit(0);} else { e.Cancel = true; }
        }

        SqlConnection conn = new SqlConnection();

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.psostav". При необходимости она может быть перемещена или удалена.
            this.psostavTableAdapter.Fill(this.metroDataSet.psostav);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.v_remont". При необходимости она может быть перемещена или удалена.
            this.v_remontTableAdapter.Fill(this.metroDataSet.v_remont);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.v_remont". При необходимости она может быть перемещена или удалена.
            this.v_remontTableAdapter.Fill(this.metroDataSet.v_remont);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.v_remont". При необходимости она может быть перемещена или удалена.
            this.v_remontTableAdapter.Fill(this.metroDataSet.v_remont);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.v_raspisanie". При необходимости она может быть перемещена или удалена.
            this.v_raspisanieTableAdapter.Fill(this.metroDataSet.v_raspisanie);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.v_raspisanie". При необходимости она может быть перемещена или удалена.
            this.v_raspisanieTableAdapter.Fill(this.metroDataSet.v_raspisanie);
            conn.ConnectionString = "Data Source=(local);" + "Initial Catalog=Metro;" + "User ID=sa;" + "Password=1950;";
            conn.Open();
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.jurnal". При необходимости она может быть перемещена или удалена.
            this.jurnalTableAdapter.Fill(this.metroDataSet.jurnal);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.spravka". При необходимости она может быть перемещена или удалена.
            this.spravkaTableAdapter.Fill(this.metroDataSet.spravka);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.remont". При необходимости она может быть перемещена или удалена.
            this.remontTableAdapter.Fill(this.metroDataSet.remont);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.mashinist". При необходимости она может быть перемещена или удалена.
            this.mashinistTableAdapter.Fill(this.metroDataSet.mashinist);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.dispetcher". При необходимости она может быть перемещена или удалена.
            this.dispetcherTableAdapter.Fill(this.metroDataSet.dispetcher);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.typsostav". При необходимости она может быть перемещена или удалена.
            this.typsostavTableAdapter.Fill(this.metroDataSet.typsostav);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.raspisanie". При необходимости она может быть перемещена или удалена.
            this.raspisanieTableAdapter.Fill(this.metroDataSet.raspisanie);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.raspisanie". При необходимости она может быть перемещена или удалена.
            this.raspisanieTableAdapter.Fill(this.metroDataSet.raspisanie);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.vetka". При необходимости она может быть перемещена или удалена.
            this.vetkaTableAdapter.Fill(this.metroDataSet.vetka);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.stanciya". При необходимости она может быть перемещена или удалена.
            this.stanciyaTableAdapter.Fill(this.metroDataSet.stanciya);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.psostav". При необходимости она может быть перемещена или удалена.
            this.psostavTableAdapter.Fill(this.metroDataSet.psostav);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "metroDataSet.raspisanie". При необходимости она может быть перемещена или удалена.
            this.raspisanieTableAdapter.Fill(this.metroDataSet.raspisanie);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            kod_vpered_stanciya.Enabled = false;
            kod_nazad_stanciya.Enabled = false;
            if (get_doljnost.Value == "Диспетчер")
            {
                tabPage10.Parent = null;
                this.dispetcherDataGridView.Columns[4].Visible = false;
                this.raspisanieDataGridView.Columns[7].Visible = false;
                this.dispetcherDataGridView.Columns[0].Visible = false;
                this.vetkaDataGridView.Size = new Size(786, 420);
                groupBox1.Visible = false;
                button_del_vetka.Visible = false;
                label18.Visible = false;
                del_name_vetka.Visible = false;
                serialpas.Visible = false;
                tel.Visible = false;
                pas.Visible = false;
                checkBox_dispetcher.Visible = false;
                doljnost.Visible = false;
                fio.Visible = false;
                label9.Visible = false;
                label11.Visible = false;
                label22.Visible = false;
                label12.Visible = false;
                label10.Visible = false;
                add_dispetcher.Visible = false;
                edit_dispetcher.Visible = false;
                del_dispetcher.Visible = false;
                groupBox10.Visible = false;
                dispetcherDataGridView.Size = new Size(790, 417);
                groupBox12.Visible = false;
                add_typik.Visible = false;
                label20.Visible = false;
                button_add_typik.Visible = false;
                button_del_typik.Visible = false;
                groupBox_stanciya.Visible = false;
                kod_stanciya.Visible = false;
                name_stanciya.Visible = false;
                paspotok_stanciya.Visible = false;
                stat_stanciya.Visible = false;
                rast_nazad_stanciya.Visible = false;
                start_time_stanciya.Visible = false;
                finish_time_stanciya.Visible = false;
                button_add_stanciya.Visible = false;
                button_edit_stanciya.Visible = false;
                button_del_stanciya.Visible = false;
                checkBox2.Visible = false;
                label23.Visible = false;
                label24.Visible = false;
                label25.Visible = false;
                label26.Visible = false;
                label27.Visible = false;
                label28.Visible = false;
                label29.Visible = false;
                label30.Visible = false;
                label31.Visible = false;
                label32.Visible = false;
                stanciyaDataGridView.Size = new Size(783, 420);
                groupBox2.Visible = false;
                label33.Visible = false;
                label34.Visible = false;
                label35.Visible = false;
                label36.Visible = false;
                label38.Visible = false;
                name_vetka.Visible = false;
                depo_status_stanciya.Visible = false;
                start_status_vetka.Visible = false;
                start_stanciya_vetka.Visible = false;
                finish_status_vetka.Visible = false;
                button_add_vetka.Visible = false;
                label13.Visible = false;
                name_vetka2.Visible = false;
            }
        }

        public void OnSqlConnectionInfoMessage(object sender, SqlInfoMessageEventArgs eventArgs)
        {
            string c = eventArgs.Message.ToString();
            MessageBox.Show(c, "Информация для пользователя", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void ClearBox(CheckBox CB, TextBox TB, string c, BindingSource BS)
        {
            if (CB.Checked == true)
            {
                TB.DataBindings.Clear();
                TB.Text = "";
            }
            else
                TB.DataBindings.Add(new System.Windows.Forms.Binding("Text", BS, c, true, DataSourceUpdateMode.OnPropertyChanged));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("prov_raspisanie", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@vetka", vetka.Text);
            insto.Parameters.AddWithValue("@n_time", start_time.Text);
            insto.Parameters.AddWithValue("@k_time", finish_time.Text);
            insto.Parameters.AddWithValue("@kol", kol.Text);
            insto.Parameters.AddWithValue("@typ", typ.Text);
            insto.Parameters.AddWithValue("@serialpas", get_login.Value);
            insto.ExecuteNonQuery();
            raspisanieTableAdapter.Fill(metroDataSet.raspisanie);
            v_raspisanieTableAdapter.Fill(metroDataSet.v_raspisanie);
            psostavTableAdapter.Fill(metroDataSet.psostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void fillToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.psostavTableAdapter.Fill(this.metroDataSet.psostav);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void del_rasp_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("del_rasp", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@vetka", vetka.Text);
            insto.ExecuteNonQuery();
            raspisanieTableAdapter.Fill(metroDataSet.raspisanie);
            v_raspisanieTableAdapter.Fill(metroDataSet.v_raspisanie);
            psostavTableAdapter.Fill(metroDataSet.psostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var insto = new SqlCommand("del_sostav_raspisanie", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@start_time", n_time.Text);
            insto.Parameters.AddWithValue("@ser", serial.Text);
            insto.ExecuteNonQuery();
            raspisanieTableAdapter.Fill(metroDataSet.raspisanie);
            v_raspisanieTableAdapter.Fill(metroDataSet.v_raspisanie);
            psostavTableAdapter.Fill(metroDataSet.psostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void add_raspis_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("add_sostav_raspis", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@vetka", add_vetka.Text);
            insto.Parameters.AddWithValue("@time", n_time.Text);
            insto.Parameters.AddWithValue("@ser", serial.Text);
            insto.Parameters.AddWithValue("@serialpas", get_login.Value);
            insto.ExecuteNonQuery();
            raspisanieTableAdapter.Fill(metroDataSet.raspisanie);
            v_raspisanieTableAdapter.Fill(metroDataSet.v_raspisanie);
            psostavTableAdapter.Fill(metroDataSet.psostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void add_dispetcher_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("add_dispetcher", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@serialpas", serialpas.Text);
            insto.Parameters.AddWithValue("@fio", fio.Text);
            insto.Parameters.AddWithValue("@tel", tel.Text);
            insto.Parameters.AddWithValue("@doljnost", doljnost.Text);
            insto.Parameters.AddWithValue("@pas", pas.Text.ToString());
            insto.ExecuteNonQuery();
            dispetcherTableAdapter.Fill(metroDataSet.dispetcher);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void del_dispetcher_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("del_dispetcher", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@serialpas", serialpas.Text);
            insto.ExecuteNonQuery();
            dispetcherTableAdapter.Fill(metroDataSet.dispetcher);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void edit_dispetcher_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("edit_dispetcher", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@serialpas", serialpas.Text);
            insto.Parameters.AddWithValue("@fio", fio.Text);
            insto.Parameters.AddWithValue("@tel", tel.Text);
            insto.Parameters.AddWithValue("@doljnost", doljnost.Text);
            insto.Parameters.AddWithValue("@pas", pas.Text);
            insto.ExecuteNonQuery();
            dispetcherTableAdapter.Fill(metroDataSet.dispetcher);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void add_mashinist_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("add_mashinist", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@serialpas", serialpas_mashinist.Text);
            insto.Parameters.AddWithValue("@serial", sostav_mashinist.Text);
            insto.Parameters.AddWithValue("@fio", fio_mashinist.Text);
            insto.Parameters.AddWithValue("@tel", tel_mashinist.Text);
            insto.Parameters.AddWithValue("@staj", staj_mashinist.Text);
            insto.Parameters.AddWithValue("@kvalif", kvalif_mashinist.Text);
            insto.ExecuteNonQuery();
            mashinistTableAdapter.Fill(metroDataSet.mashinist);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }



        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                add_mashinist.Enabled = true;
                del_mashinist.Enabled = false;
                edit_mashinist.Enabled = false;
            }
            else
            {
                add_mashinist.Enabled = false;
                del_mashinist.Enabled = true;
                edit_mashinist.Enabled = true;
            }

            ClearBox(checkBox1, serialpas_mashinist, "serialpas", mashinistBindingSource);
            ClearBox(checkBox1, kvalif_mashinist, "kvalif", mashinistBindingSource);
            ClearBox(checkBox1, tel_mashinist, "tel", mashinistBindingSource);
            ClearBox(checkBox1, fio_mashinist, "fio", mashinistBindingSource);
            ClearBox(checkBox1, staj_mashinist, "staj", mashinistBindingSource);
        }

        private void checkBox_dispetcher_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_dispetcher.Checked == true)
            {
                add_dispetcher.Enabled = true;
                del_dispetcher.Enabled = false;
                edit_dispetcher.Enabled = false;
            }
            else
            {
                add_dispetcher.Enabled = false;
                del_dispetcher.Enabled = true;
                edit_dispetcher.Enabled = true;
            }
            ClearBox(checkBox_dispetcher, serialpas, "serialpas", dispetcherBindingSource);
            ClearBox(checkBox_dispetcher, tel, "tel", dispetcherBindingSource);
            ClearBox(checkBox_dispetcher, doljnost, "doljnost", dispetcherBindingSource);
            ClearBox(checkBox_dispetcher, fio, "fio", dispetcherBindingSource);
        }

        private void checkBox_psostav_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_psostav.Checked == true)
            {
                add_psostav.Enabled = true;
                del_psostav.Enabled = false;
                edit_psostav.Enabled = false;
            }
            else
            {
                add_psostav.Enabled = false;
                del_psostav.Enabled = true;
                edit_psostav.Enabled = true;
            }
            ClearBox(checkBox_psostav, iznos_tk_psostav, "iznos_tk", psostavBindingSource);
            ClearBox(checkBox_psostav, probeg_psostav, "probeg", psostavBindingSource);
            ClearBox(checkBox_psostav, iznos_motor_psostav, "iznos_motor", psostavBindingSource);
            ClearBox(checkBox_psostav, serial_psostav, "serial", psostavBindingSource); 
        }

        private void add_psostav_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("add_sostav", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@serial", serial_psostav.Text);
            insto.Parameters.AddWithValue("@iznos_tk", iznos_tk_psostav.Text);
            insto.Parameters.AddWithValue("@vetka", stat_sostav.Text);
            insto.Parameters.AddWithValue("@typkod", typkod_psostav.Text);
            insto.Parameters.AddWithValue("@probeg", probeg_psostav.Text);
            insto.Parameters.AddWithValue("@iznos_motor", iznos_motor_psostav.Text);
            insto.ExecuteNonQuery();
            psostavTableAdapter.Fill(metroDataSet.psostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void del_psostav_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("del_sostav", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@serial", serial_psostav.Text);
            insto.ExecuteNonQuery();
            psostavTableAdapter.Fill(metroDataSet.psostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void edit_psostav_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("edit_psostav", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@serial", serial_psostav.Text);
            insto.Parameters.AddWithValue("@iznos_tk", iznos_tk_psostav.Text);
            insto.Parameters.AddWithValue("@vetka", stat_sostav.Text);
            insto.Parameters.AddWithValue("@typkod", typkod_psostav.Text);
            insto.Parameters.AddWithValue("@probeg", probeg_psostav.Text);
            insto.Parameters.AddWithValue("@iznos_motor", iznos_motor_psostav.Text);
            insto.ExecuteNonQuery();
            psostavTableAdapter.Fill(metroDataSet.psostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void alignment_interval_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("alignment_interval", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;

            raspisanieTableAdapter.Fill(metroDataSet.raspisanie);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;

        }

        private void button_add_vetka_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("add_vetka", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@name", name_vetka.Text);
            insto.Parameters.AddWithValue("@depo_status", depo_status_stanciya.Text);
            insto.Parameters.AddWithValue("@start_stanciya", start_stanciya_vetka.Text);
            insto.Parameters.AddWithValue("@start_status", start_status_vetka.Text);
            insto.Parameters.AddWithValue("@finish_status", finish_status_vetka.Text);
            insto.ExecuteNonQuery();
            vetkaTableAdapter.Fill(metroDataSet.vetka);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void button_del_vetka_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("del_vetka", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@name", del_name_vetka.Text);
            insto.Parameters.AddWithValue("@name2", name_vetka2.Text);
            insto.ExecuteNonQuery();
            vetkaTableAdapter.Fill(metroDataSet.vetka);
            raspisanieTableAdapter.Fill(metroDataSet.raspisanie);
            v_raspisanieTableAdapter.Fill(metroDataSet.v_raspisanie);
            psostavTableAdapter.Fill(metroDataSet.psostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void del_remont_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("del_remont", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@serial", serial_remont.Text);
            insto.Parameters.AddWithValue("@vetka", namevetka_remont.Text);
            insto.Parameters.AddWithValue("@serialpass", get_login.Value);
            insto.ExecuteNonQuery();
            v_remontTableAdapter.Fill(metroDataSet.v_remont);
            remontTableAdapter.Fill(metroDataSet.remont);
            psostavTableAdapter.Fill(metroDataSet.psostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void button_add_remont_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("add_remont", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@typik", typik_psostav.Text);
            insto.Parameters.AddWithValue("@zametka", zametka_psostav.Text);
            insto.Parameters.AddWithValue("@serial", serial_add_remont.Text);
            insto.Parameters.AddWithValue("@date", DateTime.Now);
            insto.Parameters.AddWithValue("@serialpass", get_login.Value);
            insto.ExecuteNonQuery();
            psostavTableAdapter.Fill(metroDataSet.psostav);
            remontTableAdapter.Fill(metroDataSet.remont);
            v_remontTableAdapter.Fill(metroDataSet.v_remont);
            jurnalTableAdapter.Fill(metroDataSet.jurnal);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void checkBox_typsostav_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_typsostav.Checked == true)
            {
                button_add_typsostav.Enabled = true;
                button_del_typsostav.Enabled = false;
                button_edit_typsostav.Enabled = false;
            }
            else
            {
                button_add_typsostav.Enabled = false;
                button_del_typsostav.Enabled = true;
                button_edit_typsostav.Enabled = true;
            }
            ClearBox(checkBox_typsostav, typkod_typsostav, "typkod", typsostavBindingSource);
            ClearBox(checkBox_typsostav, name_typsostav, "name", typsostavBindingSource);
            ClearBox(checkBox_typsostav, kolmest_typsostav, "kolmest", typsostavBindingSource);
            ClearBox(checkBox_typsostav, dlina_typsostav, "dlina", typsostavBindingSource);
            ClearBox(checkBox_typsostav, shirina_typsostav, "shirina", typsostavBindingSource);
            ClearBox(checkBox_typsostav, visota_typsostav, "visota", typsostavBindingSource);
            ClearBox(checkBox_typsostav, naznachenie_typsostav, "naznachenie", typsostavBindingSource);
            ClearBox(checkBox_typsostav, koleya_typsostav, "koleya", typsostavBindingSource);
            ClearBox(checkBox_typsostav, speed_typsostav, "speed", typsostavBindingSource);
        }

        private void button_add_typsostav_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("add_typsostav", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@typkod", typkod_typsostav.Text);
            insto.Parameters.AddWithValue("@name", name_typsostav.Text);
            insto.Parameters.AddWithValue("@kolmest", kolmest_typsostav.Text);
            insto.Parameters.AddWithValue("@dlina", dlina_typsostav.Text);
            insto.Parameters.AddWithValue("@shirina", shirina_typsostav.Text);
            insto.Parameters.AddWithValue("@visota", visota_typsostav.Text);
            insto.Parameters.AddWithValue("@naznachenie", naznachenie_typsostav.Text);
            insto.Parameters.AddWithValue("@koleya", koleya_typsostav.Text);
            insto.Parameters.AddWithValue("@speed", speed_typsostav.Text);
            insto.ExecuteNonQuery();
            typsostavTableAdapter.Fill(metroDataSet.typsostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void button_del_typsostav_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("del_typsostav", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@typkod", typkod_typsostav.Text);
            insto.ExecuteNonQuery();
            typsostavTableAdapter.Fill(metroDataSet.typsostav);
            mashinistTableAdapter.Fill(metroDataSet.mashinist);
            psostavTableAdapter.Fill(metroDataSet.psostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void button_edit_typsostav_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("edit_typsostav", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@typkod", typkod_typsostav.Text);
            insto.Parameters.AddWithValue("@name", name_typsostav.Text);
            insto.Parameters.AddWithValue("@kolmest", kolmest_typsostav.Text);
            insto.Parameters.AddWithValue("@dlina", dlina_typsostav.Text);
            insto.Parameters.AddWithValue("@shirina", shirina_typsostav.Text);
            insto.Parameters.AddWithValue("@visota", visota_typsostav.Text);
            insto.Parameters.AddWithValue("@naznachenie", naznachenie_typsostav.Text);
            insto.Parameters.AddWithValue("@koleya", koleya_typsostav.Text);
            insto.Parameters.AddWithValue("@speed", speed_typsostav.Text);
            insto.ExecuteNonQuery();
            typsostavTableAdapter.Fill(metroDataSet.typsostav);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                button_add_stanciya.Enabled = true;
                button_del_stanciya.Enabled = false;
                button_edit_stanciya.Enabled = false;
                kod_nazad_stanciya.Enabled = true;
            }
            else
            {
                button_add_stanciya.Enabled = false;
                button_del_stanciya.Enabled = true;
                button_edit_stanciya.Enabled = true;
                kod_nazad_stanciya.Enabled = false;
            }
            ClearBox(checkBox2, kod_stanciya, "kod", stanciyaBindingSource);
            ClearBox(checkBox2, name_stanciya, "name", stanciyaBindingSource);
            ClearBox(checkBox2, paspotok_stanciya, "paspotok", stanciyaBindingSource);
            ClearBox(checkBox2, stat_stanciya, "stat", stanciyaBindingSource);
            ClearBox(checkBox2, rast_nazad_stanciya, "rast_nazad", stanciyaBindingSource);
            ClearBox(checkBox2, rast_vpered_stanciya, "rast_vpered", stanciyaBindingSource);
            ClearBox(checkBox2, start_time_stanciya, "start_time", stanciyaBindingSource);
            ClearBox(checkBox2, finish_time_stanciya, "finish_time", stanciyaBindingSource);
            ClearBox(checkBox2, kod_vpered_stanciya, "kod_vpered", stanciyaBindingSource);
            ClearBox(checkBox2, kod_nazad_stanciya, "kod_nazad", stanciyaBindingSource);
        }

        private void button_edit_stanciya_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("edit_stanciya", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@kod", kod_stanciya.Text);
            insto.Parameters.AddWithValue("@name", name_stanciya.Text);
            insto.Parameters.AddWithValue("@paspotok", paspotok_stanciya.Text);
            insto.Parameters.AddWithValue("@stat", stat_stanciya.Text);
            insto.Parameters.AddWithValue("@rast_nazad", rast_nazad_stanciya.Text);
            insto.Parameters.AddWithValue("@rast_vpered", rast_vpered_stanciya.Text);
            insto.Parameters.AddWithValue("@start_time", start_time_stanciya.Text);
            insto.Parameters.AddWithValue("@finish_time", finish_time_stanciya.Text);
            insto.ExecuteNonQuery();
            stanciyaTableAdapter.Fill(metroDataSet.stanciya);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void button_add_stanciya_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("add_stanciya", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@kod", kod_stanciya.Text);
            insto.Parameters.AddWithValue("@name", name_stanciya.Text);
            insto.Parameters.AddWithValue("@paspotok", paspotok_stanciya.Text);
            insto.Parameters.AddWithValue("@stat", stat_stanciya.Text);
            insto.Parameters.AddWithValue("@rast_nazad", rast_nazad_stanciya.Text);
            insto.Parameters.AddWithValue("@rast_vpered", rast_vpered_stanciya.Text);
            insto.Parameters.AddWithValue("@start_time", start_time_stanciya.Text);
            insto.Parameters.AddWithValue("@finish_time", finish_time_stanciya.Text);
            insto.Parameters.AddWithValue("@kod_nazad", kod_nazad_stanciya.Text);
            insto.ExecuteNonQuery();
            stanciyaTableAdapter.Fill(metroDataSet.stanciya);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void button_del_stanciya_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("del_stanciya", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@kod", kod_stanciya.Text);
            insto.ExecuteNonQuery();
            stanciyaTableAdapter.Fill(metroDataSet.stanciya);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void smena_raspisanie_CheckedChanged(object sender, EventArgs e)
        {
            if (smena_raspisanie.Checked == true)
            {
                raspisanieDataGridView.Visible = true;
                v_raspisanieDataGridView.Visible = false;
            }
            else
            {
                raspisanieDataGridView.Visible = false;
                v_raspisanieDataGridView.Visible = true;
            }
        }

        private void button_add_typik_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("add_typik", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@typik", add_typik.Text);
            insto.ExecuteNonQuery();
            remontTableAdapter.Fill(metroDataSet.remont);
            v_remontTableAdapter.Fill(metroDataSet.v_remont);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void v_remont_CheckedChanged(object sender, EventArgs e)
        {
            if (v_remont.Checked == true)
            {
                remontDataGridView.Visible = true;
                v_remontDataGridView.Visible = false;
            }
            else
            {
                remontDataGridView.Visible = false;
                v_remontDataGridView.Visible = true;
            }
        }

        private void button_del_typik_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("del_typik", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@typik", add_typik.Text);
            insto.ExecuteNonQuery();
            remontTableAdapter.Fill(metroDataSet.remont);
            v_remontTableAdapter.Fill(metroDataSet.v_remont);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void edit_mashinist_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("edit_mashinist", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@serialpas", serialpas_mashinist.Text);
            insto.Parameters.AddWithValue("@serial", sostav_mashinist.Text);
            insto.Parameters.AddWithValue("@fio", fio_mashinist.Text);
            insto.Parameters.AddWithValue("@tel", tel_mashinist.Text);
            insto.Parameters.AddWithValue("@staj", staj_mashinist.Text);
            insto.Parameters.AddWithValue("@kvalif", kvalif_mashinist.Text);
            insto.ExecuteNonQuery();
            mashinistTableAdapter.Fill(metroDataSet.mashinist);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }

        private void del_mashinist_Click(object sender, EventArgs e)
        {
            var insto = new SqlCommand("del_mashinist", conn);
            conn.InfoMessage += OnSqlConnectionInfoMessage;
            insto.CommandType = CommandType.StoredProcedure;
            insto.Parameters.AddWithValue("@serialpas", serialpas_mashinist.Text);
            insto.ExecuteNonQuery();
            mashinistTableAdapter.Fill(metroDataSet.mashinist);
            conn.InfoMessage -= OnSqlConnectionInfoMessage;
        }
    }
}
