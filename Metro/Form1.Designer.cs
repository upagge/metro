﻿namespace Metro
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label serialpasLabel;
            System.Windows.Forms.Label serialLabel;
            System.Windows.Forms.Label fioLabel;
            System.Windows.Forms.Label telLabel;
            System.Windows.Forms.Label stajLabel;
            System.Windows.Forms.Label kvalifLabel;
            System.Windows.Forms.Label serialLabel1;
            System.Windows.Forms.Label typkodLabel;
            System.Windows.Forms.Label stat_psostav;
            System.Windows.Forms.Label probegLabel;
            System.Windows.Forms.Label iznos_motorLabel;
            System.Windows.Forms.Label iznos_tkLabel;
            System.Windows.Forms.Label typkodLabel1;
            System.Windows.Forms.Label nameLabel2;
            System.Windows.Forms.Label kolmestLabel;
            System.Windows.Forms.Label dlinaLabel;
            System.Windows.Forms.Label shirinaLabel;
            System.Windows.Forms.Label visotaLabel;
            System.Windows.Forms.Label naznachenieLabel;
            System.Windows.Forms.Label koleyaLabel;
            System.Windows.Forms.Label speedLabel;
            System.Windows.Forms.Label label21;
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.v_raspisanieDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn69 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.v_raspisanieBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.metroDataSet = new Metro.MetroDataSet();
            this.smena_raspisanie = new System.Windows.Forms.CheckBox();
            this.raspisanieDataGridView = new System.Windows.Forms.DataGridView();
            this.start_vremya = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.raspisanieBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.del_rasp = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.vetkaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.kol = new System.Windows.Forms.NumericUpDown();
            this.vetka = new System.Windows.Forms.ComboBox();
            this.finish_time = new System.Windows.Forms.DateTimePicker();
            this.start_time = new System.Windows.Forms.DateTimePicker();
            this.typ = new System.Windows.Forms.ComboBox();
            this.psostavBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.typsostavBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.optimal = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.serial = new System.Windows.Forms.ComboBox();
            this.add_vetka = new System.Windows.Forms.ComboBox();
            this.add_raspis = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.n_time = new System.Windows.Forms.DateTimePicker();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.checkBox_psostav = new System.Windows.Forms.CheckBox();
            this.edit_psostav = new System.Windows.Forms.Button();
            this.del_psostav = new System.Windows.Forms.Button();
            this.add_psostav = new System.Windows.Forms.Button();
            this.iznos_tk_psostav = new System.Windows.Forms.TextBox();
            this.iznos_motor_psostav = new System.Windows.Forms.TextBox();
            this.probeg_psostav = new System.Windows.Forms.TextBox();
            this.psostavDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.stat_sostav = new System.Windows.Forms.ComboBox();
            this.typkod_psostav = new System.Windows.Forms.ComboBox();
            this.serial_psostav = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.serial_add_remont = new System.Windows.Forms.ComboBox();
            this.zametka_psostav = new System.Windows.Forms.TextBox();
            this.button_add_remont = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.typik_psostav = new System.Windows.Forms.ComboBox();
            this.remontBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.del_name_vetka = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.start_stanciya_vetka = new System.Windows.Forms.TextBox();
            this.name_vetka = new System.Windows.Forms.TextBox();
            this.button_add_vetka = new System.Windows.Forms.Button();
            this.vetkaDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.name_vetka2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.button_del_vetka = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.start_status_vetka = new System.Windows.Forms.TextBox();
            this.finish_status_vetka = new System.Windows.Forms.TextBox();
            this.depo_status_stanciya = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.checkBox_typsostav = new System.Windows.Forms.CheckBox();
            this.button_del_typsostav = new System.Windows.Forms.Button();
            this.button_edit_typsostav = new System.Windows.Forms.Button();
            this.button_add_typsostav = new System.Windows.Forms.Button();
            this.speed_typsostav = new System.Windows.Forms.TextBox();
            this.koleya_typsostav = new System.Windows.Forms.TextBox();
            this.naznachenie_typsostav = new System.Windows.Forms.TextBox();
            this.visota_typsostav = new System.Windows.Forms.TextBox();
            this.shirina_typsostav = new System.Windows.Forms.TextBox();
            this.dlina_typsostav = new System.Windows.Forms.TextBox();
            this.kolmest_typsostav = new System.Windows.Forms.TextBox();
            this.name_typsostav = new System.Windows.Forms.TextBox();
            this.typkod_typsostav = new System.Windows.Forms.TextBox();
            this.typsostavDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dispetcherDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dispetcherBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.checkBox_dispetcher = new System.Windows.Forms.CheckBox();
            this.del_dispetcher = new System.Windows.Forms.Button();
            this.edit_dispetcher = new System.Windows.Forms.Button();
            this.pas = new System.Windows.Forms.TextBox();
            this.doljnost = new System.Windows.Forms.TextBox();
            this.tel = new System.Windows.Forms.TextBox();
            this.fio = new System.Windows.Forms.TextBox();
            this.serialpas = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.add_dispetcher = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.kvalif_mashinist = new System.Windows.Forms.TextBox();
            this.mashinistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.staj_mashinist = new System.Windows.Forms.TextBox();
            this.tel_mashinist = new System.Windows.Forms.TextBox();
            this.fio_mashinist = new System.Windows.Forms.TextBox();
            this.serialpas_mashinist = new System.Windows.Forms.TextBox();
            this.mashinistDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.sostav_mashinist = new System.Windows.Forms.ComboBox();
            this.del_mashinist = new System.Windows.Forms.Button();
            this.edit_mashinist = new System.Windows.Forms.Button();
            this.add_mashinist = new System.Windows.Forms.Button();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.v_remont = new System.Windows.Forms.CheckBox();
            this.v_remontDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn72 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn73 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn74 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn76 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.v_remontBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.remontDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.serial_remont = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.namevetka_remont = new System.Windows.Forms.ComboBox();
            this.del_remont = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.button_del_typik = new System.Windows.Forms.Button();
            this.button_add_typik = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.add_typik = new System.Windows.Forms.TextBox();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.spravkaDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spravkaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.button_del_stanciya = new System.Windows.Forms.Button();
            this.button_edit_stanciya = new System.Windows.Forms.Button();
            this.stat_stanciya = new System.Windows.Forms.TextBox();
            this.stanciyaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.finish_time_stanciya = new System.Windows.Forms.TextBox();
            this.start_time_stanciya = new System.Windows.Forms.TextBox();
            this.paspotok_stanciya = new System.Windows.Forms.TextBox();
            this.name_stanciya = new System.Windows.Forms.TextBox();
            this.kod_stanciya = new System.Windows.Forms.TextBox();
            this.stanciyaDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox_stanciya = new System.Windows.Forms.GroupBox();
            this.kod_nazad_stanciya = new System.Windows.Forms.TextBox();
            this.kod_vpered_stanciya = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.button_add_stanciya = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.rast_vpered_stanciya = new System.Windows.Forms.TextBox();
            this.rast_nazad_stanciya = new System.Windows.Forms.TextBox();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.jurnalDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jurnalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.raspisanieTableAdapter = new Metro.MetroDataSetTableAdapters.raspisanieTableAdapter();
            this.tableAdapterManager = new Metro.MetroDataSetTableAdapters.TableAdapterManager();
            this.psostavTableAdapter = new Metro.MetroDataSetTableAdapters.psostavTableAdapter();
            this.stanciyaTableAdapter = new Metro.MetroDataSetTableAdapters.stanciyaTableAdapter();
            this.vetkaTableAdapter = new Metro.MetroDataSetTableAdapters.vetkaTableAdapter();
            this.fKvetkaraspisanieBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fKvetkaraspisanieBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.typsostavTableAdapter = new Metro.MetroDataSetTableAdapters.typsostavTableAdapter();
            this.v_raspisanieBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dispetcherTableAdapter = new Metro.MetroDataSetTableAdapters.dispetcherTableAdapter();
            this.mashinistTableAdapter = new Metro.MetroDataSetTableAdapters.mashinistTableAdapter();
            this.remontTableAdapter = new Metro.MetroDataSetTableAdapters.remontTableAdapter();
            this.spravkaTableAdapter = new Metro.MetroDataSetTableAdapters.spravkaTableAdapter();
            this.jurnalTableAdapter = new Metro.MetroDataSetTableAdapters.jurnalTableAdapter();
            this.v_raspisanieTableAdapter = new Metro.MetroDataSetTableAdapters.v_raspisanieTableAdapter();
            this.v_remontTableAdapter = new Metro.MetroDataSetTableAdapters.v_remontTableAdapter();
            this.v_psostavBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.v_psostavBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.v_psostavBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.v_psostavBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.fKtypkodpsostavBindingSource = new System.Windows.Forms.BindingSource(this.components);
            serialpasLabel = new System.Windows.Forms.Label();
            serialLabel = new System.Windows.Forms.Label();
            fioLabel = new System.Windows.Forms.Label();
            telLabel = new System.Windows.Forms.Label();
            stajLabel = new System.Windows.Forms.Label();
            kvalifLabel = new System.Windows.Forms.Label();
            serialLabel1 = new System.Windows.Forms.Label();
            typkodLabel = new System.Windows.Forms.Label();
            stat_psostav = new System.Windows.Forms.Label();
            probegLabel = new System.Windows.Forms.Label();
            iznos_motorLabel = new System.Windows.Forms.Label();
            iznos_tkLabel = new System.Windows.Forms.Label();
            typkodLabel1 = new System.Windows.Forms.Label();
            nameLabel2 = new System.Windows.Forms.Label();
            kolmestLabel = new System.Windows.Forms.Label();
            dlinaLabel = new System.Windows.Forms.Label();
            shirinaLabel = new System.Windows.Forms.Label();
            visotaLabel = new System.Windows.Forms.Label();
            naznachenieLabel = new System.Windows.Forms.Label();
            koleyaLabel = new System.Windows.Forms.Label();
            speedLabel = new System.Windows.Forms.Label();
            label21 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_raspisanieDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_raspisanieBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.raspisanieDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.raspisanieBindingSource)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vetkaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psostavBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.typsostavBindingSource)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.psostavDataGridView)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.remontBindingSource)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vetkaDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.typsostavDataGridView)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dispetcherDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dispetcherBindingSource)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mashinistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mashinistDataGridView)).BeginInit();
            this.groupBox11.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_remontDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_remontBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.remontDataGridView)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spravkaDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spravkaBindingSource)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stanciyaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stanciyaDataGridView)).BeginInit();
            this.groupBox_stanciya.SuspendLayout();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jurnalDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jurnalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKvetkaraspisanieBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKvetkaraspisanieBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_raspisanieBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_psostavBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_psostavBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_psostavBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_psostavBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKtypkodpsostavBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // serialpasLabel
            // 
            serialpasLabel.AutoSize = true;
            serialpasLabel.Location = new System.Drawing.Point(218, 257);
            serialpasLabel.Name = "serialpasLabel";
            serialpasLabel.Size = new System.Drawing.Size(91, 13);
            serialpasLabel.TabIndex = 1;
            serialpasLabel.Text = "Номер паспорта";
            // 
            // serialLabel
            // 
            serialLabel.AutoSize = true;
            serialLabel.Location = new System.Drawing.Point(336, 302);
            serialLabel.Name = "serialLabel";
            serialLabel.Size = new System.Drawing.Size(43, 13);
            serialLabel.TabIndex = 3;
            serialLabel.Text = "Состав";
            // 
            // fioLabel
            // 
            fioLabel.AutoSize = true;
            fioLabel.Location = new System.Drawing.Point(218, 302);
            fioLabel.Name = "fioLabel";
            fioLabel.Size = new System.Drawing.Size(34, 13);
            fioLabel.TabIndex = 5;
            fioLabel.Text = "ФИО";
            // 
            // telLabel
            // 
            telLabel.AutoSize = true;
            telLabel.Location = new System.Drawing.Point(464, 257);
            telLabel.Name = "telLabel";
            telLabel.Size = new System.Drawing.Size(52, 13);
            telLabel.TabIndex = 7;
            telLabel.Text = "Телефон";
            // 
            // stajLabel
            // 
            stajLabel.AutoSize = true;
            stajLabel.Location = new System.Drawing.Point(464, 302);
            stajLabel.Name = "stajLabel";
            stajLabel.Size = new System.Drawing.Size(33, 13);
            stajLabel.TabIndex = 9;
            stajLabel.Text = "Стаж";
            // 
            // kvalifLabel
            // 
            kvalifLabel.AutoSize = true;
            kvalifLabel.Location = new System.Drawing.Point(336, 257);
            kvalifLabel.Name = "kvalifLabel";
            kvalifLabel.Size = new System.Drawing.Size(82, 13);
            kvalifLabel.TabIndex = 11;
            kvalifLabel.Text = "Квалификация";
            // 
            // serialLabel1
            // 
            serialLabel1.AutoSize = true;
            serialLabel1.Location = new System.Drawing.Point(30, 264);
            serialLabel1.Name = "serialLabel1";
            serialLabel1.Size = new System.Drawing.Size(93, 13);
            serialLabel1.TabIndex = 1;
            serialLabel1.Text = "Серийный номер";
            // 
            // typkodLabel
            // 
            typkodLabel.AutoSize = true;
            typkodLabel.Location = new System.Drawing.Point(270, 264);
            typkodLabel.Name = "typkodLabel";
            typkodLabel.Size = new System.Drawing.Size(70, 13);
            typkodLabel.TabIndex = 3;
            typkodLabel.Text = "Тип состава";
            // 
            // stat_psostav
            // 
            stat_psostav.AutoSize = true;
            stat_psostav.Location = new System.Drawing.Point(30, 301);
            stat_psostav.Name = "stat_psostav";
            stat_psostav.Size = new System.Drawing.Size(37, 13);
            stat_psostav.TabIndex = 5;
            stat_psostav.Text = "Ветка";
            // 
            // probegLabel
            // 
            probegLabel.AutoSize = true;
            probegLabel.Location = new System.Drawing.Point(270, 301);
            probegLabel.Name = "probegLabel";
            probegLabel.Size = new System.Drawing.Size(44, 13);
            probegLabel.TabIndex = 7;
            probegLabel.Text = "Пробег";
            // 
            // iznos_motorLabel
            // 
            iznos_motorLabel.AutoSize = true;
            iznos_motorLabel.Location = new System.Drawing.Point(154, 301);
            iznos_motorLabel.Name = "iznos_motorLabel";
            iznos_motorLabel.Size = new System.Drawing.Size(80, 13);
            iznos_motorLabel.TabIndex = 9;
            iznos_motorLabel.Text = "Износ Мотора";
            // 
            // iznos_tkLabel
            // 
            iznos_tkLabel.AutoSize = true;
            iznos_tkLabel.Location = new System.Drawing.Point(154, 264);
            iznos_tkLabel.Name = "iznos_tkLabel";
            iznos_tkLabel.Size = new System.Drawing.Size(99, 13);
            iznos_tkLabel.TabIndex = 11;
            iznos_tkLabel.Text = "Изношенность ТК";
            // 
            // typkodLabel1
            // 
            typkodLabel1.AutoSize = true;
            typkodLabel1.Location = new System.Drawing.Point(167, 271);
            typkodLabel1.Name = "typkodLabel1";
            typkodLabel1.Size = new System.Drawing.Size(26, 13);
            typkodLabel1.TabIndex = 1;
            typkodLabel1.Text = "Код";
            // 
            // nameLabel2
            // 
            nameLabel2.AutoSize = true;
            nameLabel2.Location = new System.Drawing.Point(167, 310);
            nameLabel2.Name = "nameLabel2";
            nameLabel2.Size = new System.Drawing.Size(57, 13);
            nameLabel2.TabIndex = 3;
            nameLabel2.Text = "Название";
            // 
            // kolmestLabel
            // 
            kolmestLabel.AutoSize = true;
            kolmestLabel.Location = new System.Drawing.Point(404, 270);
            kolmestLabel.Name = "kolmestLabel";
            kolmestLabel.Size = new System.Drawing.Size(133, 13);
            kolmestLabel.TabIndex = 5;
            kolmestLabel.Text = "Пассажировместимость";
            // 
            // dlinaLabel
            // 
            dlinaLabel.AutoSize = true;
            dlinaLabel.Location = new System.Drawing.Point(289, 271);
            dlinaLabel.Name = "dlinaLabel";
            dlinaLabel.Size = new System.Drawing.Size(40, 13);
            dlinaLabel.TabIndex = 7;
            dlinaLabel.Text = "Длина";
            // 
            // shirinaLabel
            // 
            shirinaLabel.AutoSize = true;
            shirinaLabel.Location = new System.Drawing.Point(288, 310);
            shirinaLabel.Name = "shirinaLabel";
            shirinaLabel.Size = new System.Drawing.Size(46, 13);
            shirinaLabel.TabIndex = 9;
            shirinaLabel.Text = "Ширина";
            // 
            // visotaLabel
            // 
            visotaLabel.AutoSize = true;
            visotaLabel.Location = new System.Drawing.Point(543, 270);
            visotaLabel.Name = "visotaLabel";
            visotaLabel.Size = new System.Drawing.Size(45, 13);
            visotaLabel.TabIndex = 11;
            visotaLabel.Text = "Высота";
            // 
            // naznachenieLabel
            // 
            naznachenieLabel.AutoSize = true;
            naznachenieLabel.Location = new System.Drawing.Point(167, 347);
            naznachenieLabel.Name = "naznachenieLabel";
            naznachenieLabel.Size = new System.Drawing.Size(68, 13);
            naznachenieLabel.TabIndex = 13;
            naznachenieLabel.Text = "Назначение";
            // 
            // koleyaLabel
            // 
            koleyaLabel.AutoSize = true;
            koleyaLabel.Location = new System.Drawing.Point(404, 310);
            koleyaLabel.Name = "koleyaLabel";
            koleyaLabel.Size = new System.Drawing.Size(38, 13);
            koleyaLabel.TabIndex = 15;
            koleyaLabel.Text = "Колея";
            // 
            // speedLabel
            // 
            speedLabel.AutoSize = true;
            speedLabel.Location = new System.Drawing.Point(543, 310);
            speedLabel.Name = "speedLabel";
            speedLabel.Size = new System.Drawing.Size(55, 13);
            speedLabel.TabIndex = 17;
            speedLabel.Text = "Скорость";
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Location = new System.Drawing.Point(11, 18);
            label21.Name = "label21";
            label21.Size = new System.Drawing.Size(93, 13);
            label21.TabIndex = 23;
            label21.Text = "Серийный номер";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Location = new System.Drawing.Point(2, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(797, 449);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.v_raspisanieDataGridView);
            this.tabPage1.Controls.Add(this.smena_raspisanie);
            this.tabPage1.Controls.Add(this.raspisanieDataGridView);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.kol);
            this.tabPage1.Controls.Add(this.vetka);
            this.tabPage1.Controls.Add(this.finish_time);
            this.tabPage1.Controls.Add(this.start_time);
            this.tabPage1.Controls.Add(this.typ);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.optimal);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(789, 423);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Расписание";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // v_raspisanieDataGridView
            // 
            this.v_raspisanieDataGridView.AllowUserToAddRows = false;
            this.v_raspisanieDataGridView.AllowUserToDeleteRows = false;
            this.v_raspisanieDataGridView.AutoGenerateColumns = false;
            this.v_raspisanieDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.v_raspisanieDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.v_raspisanieDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn67,
            this.dataGridViewTextBoxColumn68,
            this.dataGridViewTextBoxColumn69,
            this.dataGridViewTextBoxColumn70,
            this.dataGridViewTextBoxColumn71});
            this.v_raspisanieDataGridView.DataSource = this.v_raspisanieBindingSource1;
            this.v_raspisanieDataGridView.Location = new System.Drawing.Point(0, 0);
            this.v_raspisanieDataGridView.Name = "v_raspisanieDataGridView";
            this.v_raspisanieDataGridView.ReadOnly = true;
            this.v_raspisanieDataGridView.Size = new System.Drawing.Size(786, 267);
            this.v_raspisanieDataGridView.TabIndex = 31;
            // 
            // dataGridViewTextBoxColumn67
            // 
            this.dataGridViewTextBoxColumn67.DataPropertyName = "Отправление";
            this.dataGridViewTextBoxColumn67.HeaderText = "Отправление";
            this.dataGridViewTextBoxColumn67.Name = "dataGridViewTextBoxColumn67";
            this.dataGridViewTextBoxColumn67.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn68
            // 
            this.dataGridViewTextBoxColumn68.DataPropertyName = "Со станции";
            this.dataGridViewTextBoxColumn68.HeaderText = "Со станции";
            this.dataGridViewTextBoxColumn68.Name = "dataGridViewTextBoxColumn68";
            this.dataGridViewTextBoxColumn68.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn69
            // 
            this.dataGridViewTextBoxColumn69.DataPropertyName = "Прибытие";
            this.dataGridViewTextBoxColumn69.HeaderText = "Прибытие";
            this.dataGridViewTextBoxColumn69.Name = "dataGridViewTextBoxColumn69";
            this.dataGridViewTextBoxColumn69.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn70
            // 
            this.dataGridViewTextBoxColumn70.DataPropertyName = "На станцию";
            this.dataGridViewTextBoxColumn70.HeaderText = "На станцию";
            this.dataGridViewTextBoxColumn70.Name = "dataGridViewTextBoxColumn70";
            this.dataGridViewTextBoxColumn70.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn71
            // 
            this.dataGridViewTextBoxColumn71.DataPropertyName = "Ветка";
            this.dataGridViewTextBoxColumn71.HeaderText = "Ветка";
            this.dataGridViewTextBoxColumn71.Name = "dataGridViewTextBoxColumn71";
            this.dataGridViewTextBoxColumn71.ReadOnly = true;
            // 
            // v_raspisanieBindingSource1
            // 
            this.v_raspisanieBindingSource1.DataMember = "v_raspisanie";
            this.v_raspisanieBindingSource1.DataSource = this.metroDataSet;
            // 
            // metroDataSet
            // 
            this.metroDataSet.DataSetName = "MetroDataSet";
            this.metroDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // smena_raspisanie
            // 
            this.smena_raspisanie.AutoSize = true;
            this.smena_raspisanie.Location = new System.Drawing.Point(701, 273);
            this.smena_raspisanie.Name = "smena_raspisanie";
            this.smena_raspisanie.Size = new System.Drawing.Size(82, 17);
            this.smena_raspisanie.TabIndex = 17;
            this.smena_raspisanie.Text = "Служебное";
            this.smena_raspisanie.UseVisualStyleBackColor = true;
            this.smena_raspisanie.CheckedChanged += new System.EventHandler(this.smena_raspisanie_CheckedChanged);
            // 
            // raspisanieDataGridView
            // 
            this.raspisanieDataGridView.AllowUserToAddRows = false;
            this.raspisanieDataGridView.AllowUserToDeleteRows = false;
            this.raspisanieDataGridView.AutoGenerateColumns = false;
            this.raspisanieDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.raspisanieDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.raspisanieDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.start_vremya,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.raspisanieDataGridView.DataSource = this.raspisanieBindingSource;
            this.raspisanieDataGridView.Location = new System.Drawing.Point(0, 0);
            this.raspisanieDataGridView.Name = "raspisanieDataGridView";
            this.raspisanieDataGridView.ReadOnly = true;
            this.raspisanieDataGridView.Size = new System.Drawing.Size(786, 267);
            this.raspisanieDataGridView.TabIndex = 16;
            this.raspisanieDataGridView.Visible = false;
            // 
            // start_vremya
            // 
            this.start_vremya.DataPropertyName = "start_vremya";
            this.start_vremya.HeaderText = "start_vremya";
            this.start_vremya.Name = "start_vremya";
            this.start_vremya.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "start_kod";
            this.dataGridViewTextBoxColumn2.HeaderText = "start_kod";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "finish_vremya";
            this.dataGridViewTextBoxColumn3.HeaderText = "finish_vremya";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "finish_kod";
            this.dataGridViewTextBoxColumn4.HeaderText = "finish_kod";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "naprav";
            this.dataGridViewTextBoxColumn5.HeaderText = "naprav";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "vetka";
            this.dataGridViewTextBoxColumn6.HeaderText = "vetka";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "serial";
            this.dataGridViewTextBoxColumn7.HeaderText = "serial";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "serialpas";
            this.dataGridViewTextBoxColumn8.HeaderText = "serialpas";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // raspisanieBindingSource
            // 
            this.raspisanieBindingSource.DataMember = "raspisanie";
            this.raspisanieBindingSource.DataSource = this.metroDataSet;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label19);
            this.groupBox8.Controls.Add(this.del_rasp);
            this.groupBox8.Controls.Add(this.comboBox1);
            this.groupBox8.Location = new System.Drawing.Point(449, 355);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(334, 60);
            this.groupBox8.TabIndex = 31;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Удаление расписания";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "Ветка";
            // 
            // del_rasp
            // 
            this.del_rasp.Location = new System.Drawing.Point(167, 19);
            this.del_rasp.Name = "del_rasp";
            this.del_rasp.Size = new System.Drawing.Size(143, 32);
            this.del_rasp.TabIndex = 17;
            this.del_rasp.Text = "Удалить расписание";
            this.del_rasp.UseVisualStyleBackColor = true;
            this.del_rasp.Click += new System.EventHandler(this.del_rasp_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.vetkaBindingSource;
            this.comboBox1.DisplayMember = "name";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(6, 31);
            this.comboBox1.MaxLength = 20;
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(143, 21);
            this.comboBox1.TabIndex = 30;
            this.comboBox1.ValueMember = "name";
            // 
            // vetkaBindingSource
            // 
            this.vetkaBindingSource.DataMember = "vetka";
            this.vetkaBindingSource.DataSource = this.metroDataSet;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(361, 374);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(67, 32);
            this.button1.TabIndex = 18;
            this.button1.Text = "Удалить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // kol
            // 
            this.kol.Location = new System.Drawing.Point(416, 307);
            this.kol.Name = "kol";
            this.kol.Size = new System.Drawing.Size(60, 20);
            this.kol.TabIndex = 15;
            // 
            // vetka
            // 
            this.vetka.DataSource = this.vetkaBindingSource;
            this.vetka.DisplayMember = "name";
            this.vetka.FormattingEnabled = true;
            this.vetka.Location = new System.Drawing.Point(17, 307);
            this.vetka.MaxLength = 20;
            this.vetka.Name = "vetka";
            this.vetka.Size = new System.Drawing.Size(67, 21);
            this.vetka.TabIndex = 14;
            this.vetka.ValueMember = "name";
            // 
            // finish_time
            // 
            this.finish_time.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.finish_time.Location = new System.Drawing.Point(190, 307);
            this.finish_time.Name = "finish_time";
            this.finish_time.Size = new System.Drawing.Size(94, 20);
            this.finish_time.TabIndex = 13;
            // 
            // start_time
            // 
            this.start_time.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.start_time.Location = new System.Drawing.Point(90, 307);
            this.start_time.Name = "start_time";
            this.start_time.Size = new System.Drawing.Size(94, 20);
            this.start_time.TabIndex = 12;
            this.start_time.Value = new System.DateTime(2018, 5, 31, 7, 0, 0, 0);
            // 
            // typ
            // 
            this.typ.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.psostavBindingSource, "typkod", true));
            this.typ.DataSource = this.typsostavBindingSource;
            this.typ.DisplayMember = "typkod";
            this.typ.FormattingEnabled = true;
            this.typ.Location = new System.Drawing.Point(290, 307);
            this.typ.Name = "typ";
            this.typ.Size = new System.Drawing.Size(120, 21);
            this.typ.TabIndex = 11;
            this.typ.ValueMember = "typkod";
            // 
            // psostavBindingSource
            // 
            this.psostavBindingSource.DataMember = "psostav";
            this.psostavBindingSource.DataSource = this.metroDataSet;
            // 
            // typsostavBindingSource
            // 
            this.typsostavBindingSource.DataMember = "typsostav";
            this.typsostavBindingSource.DataSource = this.metroDataSet;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(287, 291);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Тип состава";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 292);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Ветка";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(188, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Время окончания";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 291);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Время начала";
            // 
            // optimal
            // 
            this.optimal.Location = new System.Drawing.Point(488, 292);
            this.optimal.Name = "optimal";
            this.optimal.Size = new System.Drawing.Size(151, 35);
            this.optimal.TabIndex = 1;
            this.optimal.Text = "Оптимальное расписание";
            this.optimal.UseVisualStyleBackColor = true;
            this.optimal.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.serial);
            this.groupBox6.Controls.Add(this.add_vetka);
            this.groupBox6.Controls.Add(this.add_raspis);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.n_time);
            this.groupBox6.Location = new System.Drawing.Point(6, 355);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(436, 62);
            this.groupBox6.TabIndex = 28;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Изменение расписания";
            // 
            // serial
            // 
            this.serial.DataSource = this.psostavBindingSource;
            this.serial.DisplayMember = "serial";
            this.serial.FormattingEnabled = true;
            this.serial.Location = new System.Drawing.Point(182, 31);
            this.serial.Name = "serial";
            this.serial.Size = new System.Drawing.Size(75, 21);
            this.serial.TabIndex = 26;
            this.serial.ValueMember = "serial";
            // 
            // add_vetka
            // 
            this.add_vetka.DataSource = this.vetkaBindingSource;
            this.add_vetka.DisplayMember = "name";
            this.add_vetka.FormattingEnabled = true;
            this.add_vetka.Location = new System.Drawing.Point(9, 31);
            this.add_vetka.MaxLength = 20;
            this.add_vetka.Name = "add_vetka";
            this.add_vetka.Size = new System.Drawing.Size(67, 21);
            this.add_vetka.TabIndex = 25;
            this.add_vetka.ValueMember = "name";
            // 
            // add_raspis
            // 
            this.add_raspis.Location = new System.Drawing.Point(278, 19);
            this.add_raspis.Name = "add_raspis";
            this.add_raspis.Size = new System.Drawing.Size(71, 32);
            this.add_raspis.TabIndex = 23;
            this.add_raspis.Text = "Добавить";
            this.add_raspis.UseVisualStyleBackColor = true;
            this.add_raspis.Click += new System.EventHandler(this.add_raspis_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Ветка";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(79, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Время";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(179, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Серийный номер";
            // 
            // n_time
            // 
            this.n_time.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.n_time.Location = new System.Drawing.Point(82, 32);
            this.n_time.Name = "n_time";
            this.n_time.Size = new System.Drawing.Size(94, 20);
            this.n_time.TabIndex = 21;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Location = new System.Drawing.Point(6, 273);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(639, 76);
            this.groupBox7.TabIndex = 29;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Формирование основного расписания";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(407, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Количество";
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.checkBox_psostav);
            this.tabPage2.Controls.Add(this.edit_psostav);
            this.tabPage2.Controls.Add(this.del_psostav);
            this.tabPage2.Controls.Add(this.add_psostav);
            this.tabPage2.Controls.Add(iznos_tkLabel);
            this.tabPage2.Controls.Add(this.iznos_tk_psostav);
            this.tabPage2.Controls.Add(iznos_motorLabel);
            this.tabPage2.Controls.Add(this.iznos_motor_psostav);
            this.tabPage2.Controls.Add(probegLabel);
            this.tabPage2.Controls.Add(this.probeg_psostav);
            this.tabPage2.Controls.Add(stat_psostav);
            this.tabPage2.Controls.Add(typkodLabel);
            this.tabPage2.Controls.Add(serialLabel1);
            this.tabPage2.Controls.Add(this.psostavDataGridView);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(789, 423);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Составы";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // checkBox_psostav
            // 
            this.checkBox_psostav.AutoSize = true;
            this.checkBox_psostav.Location = new System.Drawing.Point(33, 389);
            this.checkBox_psostav.Name = "checkBox_psostav";
            this.checkBox_psostav.Size = new System.Drawing.Size(134, 17);
            this.checkBox_psostav.TabIndex = 16;
            this.checkBox_psostav.Text = "Отвязать от таблицы";
            this.checkBox_psostav.UseVisualStyleBackColor = true;
            this.checkBox_psostav.CheckedChanged += new System.EventHandler(this.checkBox_psostav_CheckedChanged);
            // 
            // edit_psostav
            // 
            this.edit_psostav.Location = new System.Drawing.Point(150, 343);
            this.edit_psostav.Name = "edit_psostav";
            this.edit_psostav.Size = new System.Drawing.Size(114, 40);
            this.edit_psostav.TabIndex = 15;
            this.edit_psostav.Text = "Изменить";
            this.edit_psostav.UseVisualStyleBackColor = true;
            this.edit_psostav.Click += new System.EventHandler(this.edit_psostav_Click);
            // 
            // del_psostav
            // 
            this.del_psostav.Location = new System.Drawing.Point(270, 343);
            this.del_psostav.Name = "del_psostav";
            this.del_psostav.Size = new System.Drawing.Size(103, 40);
            this.del_psostav.TabIndex = 14;
            this.del_psostav.Text = "Удалить";
            this.del_psostav.UseVisualStyleBackColor = true;
            this.del_psostav.Click += new System.EventHandler(this.del_psostav_Click);
            // 
            // add_psostav
            // 
            this.add_psostav.Enabled = false;
            this.add_psostav.Location = new System.Drawing.Point(33, 343);
            this.add_psostav.Name = "add_psostav";
            this.add_psostav.Size = new System.Drawing.Size(111, 40);
            this.add_psostav.TabIndex = 13;
            this.add_psostav.Text = "Добавить";
            this.add_psostav.UseVisualStyleBackColor = true;
            this.add_psostav.Click += new System.EventHandler(this.add_psostav_Click);
            // 
            // iznos_tk_psostav
            // 
            this.iznos_tk_psostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.psostavBindingSource, "iznos_tk", true));
            this.iznos_tk_psostav.Location = new System.Drawing.Point(157, 280);
            this.iznos_tk_psostav.Name = "iznos_tk_psostav";
            this.iznos_tk_psostav.Size = new System.Drawing.Size(100, 20);
            this.iznos_tk_psostav.TabIndex = 12;
            // 
            // iznos_motor_psostav
            // 
            this.iznos_motor_psostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.psostavBindingSource, "iznos_motor", true));
            this.iznos_motor_psostav.Location = new System.Drawing.Point(157, 317);
            this.iznos_motor_psostav.Name = "iznos_motor_psostav";
            this.iznos_motor_psostav.Size = new System.Drawing.Size(100, 20);
            this.iznos_motor_psostav.TabIndex = 10;
            // 
            // probeg_psostav
            // 
            this.probeg_psostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.psostavBindingSource, "probeg", true));
            this.probeg_psostav.Location = new System.Drawing.Point(273, 317);
            this.probeg_psostav.Name = "probeg_psostav";
            this.probeg_psostav.Size = new System.Drawing.Size(100, 20);
            this.probeg_psostav.TabIndex = 8;
            // 
            // psostavDataGridView
            // 
            this.psostavDataGridView.AllowUserToAddRows = false;
            this.psostavDataGridView.AllowUserToDeleteRows = false;
            this.psostavDataGridView.AutoGenerateColumns = false;
            this.psostavDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.psostavDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.psostavDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14});
            this.psostavDataGridView.DataSource = this.psostavBindingSource;
            this.psostavDataGridView.Location = new System.Drawing.Point(0, 0);
            this.psostavDataGridView.Name = "psostavDataGridView";
            this.psostavDataGridView.ReadOnly = true;
            this.psostavDataGridView.Size = new System.Drawing.Size(786, 224);
            this.psostavDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "serial";
            this.dataGridViewTextBoxColumn9.HeaderText = "serial";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "typkod";
            this.dataGridViewTextBoxColumn10.HeaderText = "typkod";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "stat";
            this.dataGridViewTextBoxColumn11.HeaderText = "stat";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "probeg";
            this.dataGridViewTextBoxColumn12.HeaderText = "probeg";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "iznos_motor";
            this.dataGridViewTextBoxColumn13.HeaderText = "iznos_motor";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "iznos_tk";
            this.dataGridViewTextBoxColumn14.HeaderText = "iznos_tk";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.stat_sostav);
            this.groupBox3.Controls.Add(this.typkod_psostav);
            this.groupBox3.Controls.Add(this.serial_psostav);
            this.groupBox3.Location = new System.Drawing.Point(21, 246);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(369, 169);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Изменение данных";
            // 
            // stat_sostav
            // 
            this.stat_sostav.DataSource = this.vetkaBindingSource;
            this.stat_sostav.DisplayMember = "name";
            this.stat_sostav.FormattingEnabled = true;
            this.stat_sostav.Location = new System.Drawing.Point(12, 71);
            this.stat_sostav.Name = "stat_sostav";
            this.stat_sostav.Size = new System.Drawing.Size(100, 21);
            this.stat_sostav.TabIndex = 27;
            this.stat_sostav.ValueMember = "name";
            // 
            // typkod_psostav
            // 
            this.typkod_psostav.DataSource = this.typsostavBindingSource;
            this.typkod_psostav.DisplayMember = "typkod";
            this.typkod_psostav.FormattingEnabled = true;
            this.typkod_psostav.Location = new System.Drawing.Point(252, 34);
            this.typkod_psostav.Name = "typkod_psostav";
            this.typkod_psostav.Size = new System.Drawing.Size(100, 21);
            this.typkod_psostav.TabIndex = 26;
            this.typkod_psostav.ValueMember = "typkod";
            // 
            // serial_psostav
            // 
            this.serial_psostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.psostavBindingSource, "serial", true));
            this.serial_psostav.Location = new System.Drawing.Point(12, 34);
            this.serial_psostav.Name = "serial_psostav";
            this.serial_psostav.Size = new System.Drawing.Size(100, 20);
            this.serial_psostav.TabIndex = 25;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.serial_add_remont);
            this.groupBox4.Controls.Add(label21);
            this.groupBox4.Controls.Add(this.zametka_psostav);
            this.groupBox4.Controls.Add(this.button_add_remont);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.typik_psostav);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(416, 246);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(353, 120);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Техническое обслуживание";
            // 
            // serial_add_remont
            // 
            this.serial_add_remont.DataSource = this.psostavBindingSource;
            this.serial_add_remont.DisplayMember = "serial";
            this.serial_add_remont.FormattingEnabled = true;
            this.serial_add_remont.Location = new System.Drawing.Point(11, 37);
            this.serial_add_remont.Name = "serial_add_remont";
            this.serial_add_remont.Size = new System.Drawing.Size(115, 21);
            this.serial_add_remont.TabIndex = 24;
            this.serial_add_remont.ValueMember = "serial";
            // 
            // zametka_psostav
            // 
            this.zametka_psostav.Location = new System.Drawing.Point(144, 33);
            this.zametka_psostav.Name = "zametka_psostav";
            this.zametka_psostav.Size = new System.Drawing.Size(191, 20);
            this.zametka_psostav.TabIndex = 22;
            // 
            // button_add_remont
            // 
            this.button_add_remont.Location = new System.Drawing.Point(144, 60);
            this.button_add_remont.Name = "button_add_remont";
            this.button_add_remont.Size = new System.Drawing.Size(191, 40);
            this.button_add_remont.TabIndex = 17;
            this.button_add_remont.Text = "Отправить в ремонт";
            this.button_add_remont.UseVisualStyleBackColor = true;
            this.button_add_remont.Click += new System.EventHandler(this.button_add_remont_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(144, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "Заметка";
            // 
            // typik_psostav
            // 
            this.typik_psostav.DataSource = this.remontBindingSource;
            this.typik_psostav.DisplayMember = "typik";
            this.typik_psostav.FormattingEnabled = true;
            this.typik_psostav.Location = new System.Drawing.Point(11, 77);
            this.typik_psostav.Name = "typik_psostav";
            this.typik_psostav.Size = new System.Drawing.Size(115, 21);
            this.typik_psostav.TabIndex = 18;
            this.typik_psostav.ValueMember = "typik";
            // 
            // remontBindingSource
            // 
            this.remontBindingSource.DataMember = "remont";
            this.remontBindingSource.DataSource = this.metroDataSet;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 61);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "Ремонтный Тупик";
            // 
            // tabPage3
            // 
            this.tabPage3.AutoScroll = true;
            this.tabPage3.Controls.Add(this.del_name_vetka);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.start_stanciya_vetka);
            this.tabPage3.Controls.Add(this.name_vetka);
            this.tabPage3.Controls.Add(this.button_add_vetka);
            this.tabPage3.Controls.Add(this.vetkaDataGridView);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(789, 423);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Ветки";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // del_name_vetka
            // 
            this.del_name_vetka.DataSource = this.vetkaBindingSource;
            this.del_name_vetka.DisplayMember = "name";
            this.del_name_vetka.FormattingEnabled = true;
            this.del_name_vetka.Location = new System.Drawing.Point(466, 342);
            this.del_name_vetka.Name = "del_name_vetka";
            this.del_name_vetka.Size = new System.Drawing.Size(121, 21);
            this.del_name_vetka.TabIndex = 26;
            this.del_name_vetka.ValueMember = "name";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(463, 326);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(97, 13);
            this.label18.TabIndex = 25;
            this.label18.Text = "Удаляемая ветка";
            // 
            // start_stanciya_vetka
            // 
            this.start_stanciya_vetka.Location = new System.Drawing.Point(44, 381);
            this.start_stanciya_vetka.Name = "start_stanciya_vetka";
            this.start_stanciya_vetka.Size = new System.Drawing.Size(100, 20);
            this.start_stanciya_vetka.TabIndex = 22;
            // 
            // name_vetka
            // 
            this.name_vetka.Location = new System.Drawing.Point(44, 336);
            this.name_vetka.Name = "name_vetka";
            this.name_vetka.Size = new System.Drawing.Size(100, 20);
            this.name_vetka.TabIndex = 15;
            // 
            // button_add_vetka
            // 
            this.button_add_vetka.Location = new System.Drawing.Point(283, 365);
            this.button_add_vetka.Name = "button_add_vetka";
            this.button_add_vetka.Size = new System.Drawing.Size(105, 36);
            this.button_add_vetka.TabIndex = 13;
            this.button_add_vetka.Text = "Добавить";
            this.button_add_vetka.UseVisualStyleBackColor = true;
            this.button_add_vetka.Click += new System.EventHandler(this.button_add_vetka_Click);
            // 
            // vetkaDataGridView
            // 
            this.vetkaDataGridView.AllowUserToAddRows = false;
            this.vetkaDataGridView.AllowUserToDeleteRows = false;
            this.vetkaDataGridView.AutoGenerateColumns = false;
            this.vetkaDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.vetkaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vetkaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20});
            this.vetkaDataGridView.DataSource = this.vetkaBindingSource;
            this.vetkaDataGridView.Location = new System.Drawing.Point(0, 0);
            this.vetkaDataGridView.Name = "vetkaDataGridView";
            this.vetkaDataGridView.ReadOnly = true;
            this.vetkaDataGridView.Size = new System.Drawing.Size(786, 295);
            this.vetkaDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn15.HeaderText = "name";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "start_stanciya";
            this.dataGridViewTextBoxColumn16.HeaderText = "start_stanciya";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "finish_stanciya";
            this.dataGridViewTextBoxColumn17.HeaderText = "finish_stanciya";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "depo_status";
            this.dataGridViewTextBoxColumn18.HeaderText = "depo_status";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "start_status";
            this.dataGridViewTextBoxColumn19.HeaderText = "start_status";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "finish_status";
            this.dataGridViewTextBoxColumn20.HeaderText = "finish_status";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.name_vetka2);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.button_del_vetka);
            this.groupBox1.Location = new System.Drawing.Point(454, 301);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(272, 114);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Удаление ветки";
            // 
            // name_vetka2
            // 
            this.name_vetka2.Location = new System.Drawing.Point(139, 41);
            this.name_vetka2.Name = "name_vetka2";
            this.name_vetka2.Size = new System.Drawing.Size(121, 20);
            this.name_vetka2.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(136, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(126, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Переносим составы на";
            // 
            // button_del_vetka
            // 
            this.button_del_vetka.Location = new System.Drawing.Point(12, 68);
            this.button_del_vetka.Name = "button_del_vetka";
            this.button_del_vetka.Size = new System.Drawing.Size(248, 32);
            this.button_del_vetka.TabIndex = 14;
            this.button_del_vetka.Text = "Удалить";
            this.button_del_vetka.UseVisualStyleBackColor = true;
            this.button_del_vetka.Click += new System.EventHandler(this.button_del_vetka_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.start_status_vetka);
            this.groupBox2.Controls.Add(this.finish_status_vetka);
            this.groupBox2.Controls.Add(this.depo_status_stanciya);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Location = new System.Drawing.Point(34, 301);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(389, 114);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Добавление ветки";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(113, 64);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(130, 13);
            this.label38.TabIndex = 5;
            this.label38.Text = "Статус движения к депо";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(7, 64);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(106, 13);
            this.label36.TabIndex = 3;
            this.label36.Text = "Начальная станция";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(219, 19);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(135, 13);
            this.label35.TabIndex = 2;
            this.label35.Text = "Статус движения от депо";
            // 
            // start_status_vetka
            // 
            this.start_status_vetka.Location = new System.Drawing.Point(222, 35);
            this.start_status_vetka.Name = "start_status_vetka";
            this.start_status_vetka.Size = new System.Drawing.Size(100, 20);
            this.start_status_vetka.TabIndex = 19;
            // 
            // finish_status_vetka
            // 
            this.finish_status_vetka.Location = new System.Drawing.Point(116, 80);
            this.finish_status_vetka.Name = "finish_status_vetka";
            this.finish_status_vetka.Size = new System.Drawing.Size(100, 20);
            this.finish_status_vetka.TabIndex = 20;
            // 
            // depo_status_stanciya
            // 
            this.depo_status_stanciya.Location = new System.Drawing.Point(116, 35);
            this.depo_status_stanciya.Name = "depo_status_stanciya";
            this.depo_status_stanciya.Size = new System.Drawing.Size(100, 20);
            this.depo_status_stanciya.TabIndex = 16;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(113, 19);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 13);
            this.label34.TabIndex = 1;
            this.label34.Text = "Код депо";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(7, 19);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(57, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "Название";
            // 
            // tabPage4
            // 
            this.tabPage4.AutoScroll = true;
            this.tabPage4.Controls.Add(this.checkBox_typsostav);
            this.tabPage4.Controls.Add(this.button_del_typsostav);
            this.tabPage4.Controls.Add(this.button_edit_typsostav);
            this.tabPage4.Controls.Add(this.button_add_typsostav);
            this.tabPage4.Controls.Add(speedLabel);
            this.tabPage4.Controls.Add(this.speed_typsostav);
            this.tabPage4.Controls.Add(koleyaLabel);
            this.tabPage4.Controls.Add(this.koleya_typsostav);
            this.tabPage4.Controls.Add(naznachenieLabel);
            this.tabPage4.Controls.Add(this.naznachenie_typsostav);
            this.tabPage4.Controls.Add(visotaLabel);
            this.tabPage4.Controls.Add(this.visota_typsostav);
            this.tabPage4.Controls.Add(shirinaLabel);
            this.tabPage4.Controls.Add(this.shirina_typsostav);
            this.tabPage4.Controls.Add(dlinaLabel);
            this.tabPage4.Controls.Add(this.dlina_typsostav);
            this.tabPage4.Controls.Add(kolmestLabel);
            this.tabPage4.Controls.Add(this.kolmest_typsostav);
            this.tabPage4.Controls.Add(nameLabel2);
            this.tabPage4.Controls.Add(this.name_typsostav);
            this.tabPage4.Controls.Add(typkodLabel1);
            this.tabPage4.Controls.Add(this.typkod_typsostav);
            this.tabPage4.Controls.Add(this.typsostavDataGridView);
            this.tabPage4.Controls.Add(this.groupBox9);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(789, 423);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Типы составов";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // checkBox_typsostav
            // 
            this.checkBox_typsostav.AutoSize = true;
            this.checkBox_typsostav.Location = new System.Drawing.Point(319, 390);
            this.checkBox_typsostav.Name = "checkBox_typsostav";
            this.checkBox_typsostav.Size = new System.Drawing.Size(127, 17);
            this.checkBox_typsostav.TabIndex = 22;
            this.checkBox_typsostav.Text = "Отвязать от формы";
            this.checkBox_typsostav.UseVisualStyleBackColor = true;
            this.checkBox_typsostav.CheckedChanged += new System.EventHandler(this.checkBox_typsostav_CheckedChanged);
            // 
            // button_del_typsostav
            // 
            this.button_del_typsostav.Location = new System.Drawing.Point(543, 355);
            this.button_del_typsostav.Name = "button_del_typsostav";
            this.button_del_typsostav.Size = new System.Drawing.Size(102, 29);
            this.button_del_typsostav.TabIndex = 21;
            this.button_del_typsostav.Text = "Удалить";
            this.button_del_typsostav.UseVisualStyleBackColor = true;
            this.button_del_typsostav.Click += new System.EventHandler(this.button_del_typsostav_Click);
            // 
            // button_edit_typsostav
            // 
            this.button_edit_typsostav.Location = new System.Drawing.Point(437, 355);
            this.button_edit_typsostav.Name = "button_edit_typsostav";
            this.button_edit_typsostav.Size = new System.Drawing.Size(100, 29);
            this.button_edit_typsostav.TabIndex = 20;
            this.button_edit_typsostav.Text = "Изменить";
            this.button_edit_typsostav.UseVisualStyleBackColor = true;
            this.button_edit_typsostav.Click += new System.EventHandler(this.button_edit_typsostav_Click);
            // 
            // button_add_typsostav
            // 
            this.button_add_typsostav.Enabled = false;
            this.button_add_typsostav.Location = new System.Drawing.Point(319, 355);
            this.button_add_typsostav.Name = "button_add_typsostav";
            this.button_add_typsostav.Size = new System.Drawing.Size(112, 29);
            this.button_add_typsostav.TabIndex = 19;
            this.button_add_typsostav.Text = "Добавить";
            this.button_add_typsostav.UseVisualStyleBackColor = true;
            this.button_add_typsostav.Click += new System.EventHandler(this.button_add_typsostav_Click);
            // 
            // speed_typsostav
            // 
            this.speed_typsostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.typsostavBindingSource, "speed", true));
            this.speed_typsostav.Location = new System.Drawing.Point(545, 326);
            this.speed_typsostav.Name = "speed_typsostav";
            this.speed_typsostav.Size = new System.Drawing.Size(100, 20);
            this.speed_typsostav.TabIndex = 18;
            // 
            // koleya_typsostav
            // 
            this.koleya_typsostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.typsostavBindingSource, "koleya", true));
            this.koleya_typsostav.Location = new System.Drawing.Point(407, 324);
            this.koleya_typsostav.Name = "koleya_typsostav";
            this.koleya_typsostav.Size = new System.Drawing.Size(100, 20);
            this.koleya_typsostav.TabIndex = 16;
            // 
            // naznachenie_typsostav
            // 
            this.naznachenie_typsostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.typsostavBindingSource, "naznachenie", true));
            this.naznachenie_typsostav.Location = new System.Drawing.Point(170, 364);
            this.naznachenie_typsostav.Name = "naznachenie_typsostav";
            this.naznachenie_typsostav.Size = new System.Drawing.Size(130, 20);
            this.naznachenie_typsostav.TabIndex = 14;
            // 
            // visota_typsostav
            // 
            this.visota_typsostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.typsostavBindingSource, "visota", true));
            this.visota_typsostav.Location = new System.Drawing.Point(546, 284);
            this.visota_typsostav.Name = "visota_typsostav";
            this.visota_typsostav.Size = new System.Drawing.Size(100, 20);
            this.visota_typsostav.TabIndex = 12;
            // 
            // shirina_typsostav
            // 
            this.shirina_typsostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.typsostavBindingSource, "shirina", true));
            this.shirina_typsostav.Location = new System.Drawing.Point(291, 324);
            this.shirina_typsostav.Name = "shirina_typsostav";
            this.shirina_typsostav.Size = new System.Drawing.Size(100, 20);
            this.shirina_typsostav.TabIndex = 10;
            // 
            // dlina_typsostav
            // 
            this.dlina_typsostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.typsostavBindingSource, "dlina", true));
            this.dlina_typsostav.Location = new System.Drawing.Point(292, 288);
            this.dlina_typsostav.Name = "dlina_typsostav";
            this.dlina_typsostav.Size = new System.Drawing.Size(100, 20);
            this.dlina_typsostav.TabIndex = 8;
            // 
            // kolmest_typsostav
            // 
            this.kolmest_typsostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.typsostavBindingSource, "kolmest", true));
            this.kolmest_typsostav.Location = new System.Drawing.Point(407, 285);
            this.kolmest_typsostav.Name = "kolmest_typsostav";
            this.kolmest_typsostav.Size = new System.Drawing.Size(100, 20);
            this.kolmest_typsostav.TabIndex = 6;
            // 
            // name_typsostav
            // 
            this.name_typsostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.typsostavBindingSource, "name", true));
            this.name_typsostav.Location = new System.Drawing.Point(170, 324);
            this.name_typsostav.Name = "name_typsostav";
            this.name_typsostav.Size = new System.Drawing.Size(100, 20);
            this.name_typsostav.TabIndex = 4;
            // 
            // typkod_typsostav
            // 
            this.typkod_typsostav.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.typsostavBindingSource, "typkod", true));
            this.typkod_typsostav.Location = new System.Drawing.Point(170, 287);
            this.typkod_typsostav.Name = "typkod_typsostav";
            this.typkod_typsostav.Size = new System.Drawing.Size(100, 20);
            this.typkod_typsostav.TabIndex = 2;
            // 
            // typsostavDataGridView
            // 
            this.typsostavDataGridView.AllowUserToAddRows = false;
            this.typsostavDataGridView.AllowUserToDeleteRows = false;
            this.typsostavDataGridView.AutoGenerateColumns = false;
            this.typsostavDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.typsostavDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.typsostavDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29});
            this.typsostavDataGridView.DataSource = this.typsostavBindingSource;
            this.typsostavDataGridView.Location = new System.Drawing.Point(0, 0);
            this.typsostavDataGridView.Name = "typsostavDataGridView";
            this.typsostavDataGridView.ReadOnly = true;
            this.typsostavDataGridView.Size = new System.Drawing.Size(786, 247);
            this.typsostavDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "typkod";
            this.dataGridViewTextBoxColumn21.HeaderText = "typkod";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn22.HeaderText = "name";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "kolmest";
            this.dataGridViewTextBoxColumn23.HeaderText = "kolmest";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "dlina";
            this.dataGridViewTextBoxColumn24.HeaderText = "dlina";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "shirina";
            this.dataGridViewTextBoxColumn25.HeaderText = "shirina";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "visota";
            this.dataGridViewTextBoxColumn26.HeaderText = "visota";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "naznachenie";
            this.dataGridViewTextBoxColumn27.HeaderText = "naznachenie";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "koleya";
            this.dataGridViewTextBoxColumn28.HeaderText = "koleya";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "speed";
            this.dataGridViewTextBoxColumn29.HeaderText = "speed";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Location = new System.Drawing.Point(154, 253);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(526, 162);
            this.groupBox9.TabIndex = 23;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Управление типами";
            // 
            // tabPage5
            // 
            this.tabPage5.AutoScroll = true;
            this.tabPage5.Controls.Add(this.dispetcherDataGridView);
            this.tabPage5.Controls.Add(this.checkBox_dispetcher);
            this.tabPage5.Controls.Add(this.del_dispetcher);
            this.tabPage5.Controls.Add(this.edit_dispetcher);
            this.tabPage5.Controls.Add(this.pas);
            this.tabPage5.Controls.Add(this.doljnost);
            this.tabPage5.Controls.Add(this.tel);
            this.tabPage5.Controls.Add(this.fio);
            this.tabPage5.Controls.Add(this.serialpas);
            this.tabPage5.Controls.Add(this.label12);
            this.tabPage5.Controls.Add(this.label11);
            this.tabPage5.Controls.Add(this.label10);
            this.tabPage5.Controls.Add(this.add_dispetcher);
            this.tabPage5.Controls.Add(this.label9);
            this.tabPage5.Controls.Add(this.groupBox10);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(789, 423);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Диспетчеры";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dispetcherDataGridView
            // 
            this.dispetcherDataGridView.AllowUserToAddRows = false;
            this.dispetcherDataGridView.AllowUserToDeleteRows = false;
            this.dispetcherDataGridView.AutoGenerateColumns = false;
            this.dispetcherDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dispetcherDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dispetcherDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34});
            this.dispetcherDataGridView.DataSource = this.dispetcherBindingSource;
            this.dispetcherDataGridView.Location = new System.Drawing.Point(-4, 3);
            this.dispetcherDataGridView.Name = "dispetcherDataGridView";
            this.dispetcherDataGridView.ReadOnly = true;
            this.dispetcherDataGridView.Size = new System.Drawing.Size(790, 234);
            this.dispetcherDataGridView.TabIndex = 19;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "serialpas";
            this.dataGridViewTextBoxColumn30.HeaderText = "serialpas";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "fio";
            this.dataGridViewTextBoxColumn31.HeaderText = "fio";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "tel";
            this.dataGridViewTextBoxColumn32.HeaderText = "tel";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "doljnost";
            this.dataGridViewTextBoxColumn33.HeaderText = "doljnost";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "pas";
            this.dataGridViewTextBoxColumn34.HeaderText = "pas";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            // 
            // dispetcherBindingSource
            // 
            this.dispetcherBindingSource.DataMember = "dispetcher";
            this.dispetcherBindingSource.DataSource = this.metroDataSet;
            // 
            // checkBox_dispetcher
            // 
            this.checkBox_dispetcher.AutoSize = true;
            this.checkBox_dispetcher.Location = new System.Drawing.Point(185, 333);
            this.checkBox_dispetcher.Name = "checkBox_dispetcher";
            this.checkBox_dispetcher.Size = new System.Drawing.Size(117, 17);
            this.checkBox_dispetcher.TabIndex = 19;
            this.checkBox_dispetcher.Text = "Отвязать от базы";
            this.checkBox_dispetcher.UseVisualStyleBackColor = true;
            this.checkBox_dispetcher.CheckedChanged += new System.EventHandler(this.checkBox_dispetcher_CheckedChanged);
            // 
            // del_dispetcher
            // 
            this.del_dispetcher.Location = new System.Drawing.Point(460, 356);
            this.del_dispetcher.Name = "del_dispetcher";
            this.del_dispetcher.Size = new System.Drawing.Size(150, 38);
            this.del_dispetcher.TabIndex = 18;
            this.del_dispetcher.Text = "Удалить";
            this.del_dispetcher.UseVisualStyleBackColor = true;
            this.del_dispetcher.Click += new System.EventHandler(this.del_dispetcher_Click);
            // 
            // edit_dispetcher
            // 
            this.edit_dispetcher.Location = new System.Drawing.Point(325, 356);
            this.edit_dispetcher.Name = "edit_dispetcher";
            this.edit_dispetcher.Size = new System.Drawing.Size(129, 38);
            this.edit_dispetcher.TabIndex = 17;
            this.edit_dispetcher.Text = "Изменить";
            this.edit_dispetcher.UseVisualStyleBackColor = true;
            this.edit_dispetcher.Click += new System.EventHandler(this.edit_dispetcher_Click);
            // 
            // pas
            // 
            this.pas.Location = new System.Drawing.Point(491, 284);
            this.pas.Name = "pas";
            this.pas.Size = new System.Drawing.Size(119, 20);
            this.pas.TabIndex = 16;
            // 
            // doljnost
            // 
            this.doljnost.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dispetcherBindingSource, "doljnost", true));
            this.doljnost.Location = new System.Drawing.Point(332, 325);
            this.doljnost.Name = "doljnost";
            this.doljnost.Size = new System.Drawing.Size(140, 20);
            this.doljnost.TabIndex = 15;
            // 
            // tel
            // 
            this.tel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dispetcherBindingSource, "tel", true));
            this.tel.Location = new System.Drawing.Point(332, 286);
            this.tel.Name = "tel";
            this.tel.Size = new System.Drawing.Size(140, 20);
            this.tel.TabIndex = 14;
            // 
            // fio
            // 
            this.fio.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dispetcherBindingSource, "fio", true));
            this.fio.Location = new System.Drawing.Point(491, 325);
            this.fio.Name = "fio";
            this.fio.Size = new System.Drawing.Size(119, 20);
            this.fio.TabIndex = 13;
            // 
            // serialpas
            // 
            this.serialpas.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dispetcherBindingSource, "serialpas", true));
            this.serialpas.Location = new System.Drawing.Point(183, 286);
            this.serialpas.Name = "serialpas";
            this.serialpas.Size = new System.Drawing.Size(119, 20);
            this.serialpas.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(329, 309);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Должность";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(329, 268);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Телефон";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(488, 309);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "ФИО";
            // 
            // add_dispetcher
            // 
            this.add_dispetcher.Enabled = false;
            this.add_dispetcher.Location = new System.Drawing.Point(183, 356);
            this.add_dispetcher.Name = "add_dispetcher";
            this.add_dispetcher.Size = new System.Drawing.Size(136, 38);
            this.add_dispetcher.TabIndex = 3;
            this.add_dispetcher.Text = "Добавить";
            this.add_dispetcher.UseVisualStyleBackColor = true;
            this.add_dispetcher.Click += new System.EventHandler(this.add_dispetcher_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(180, 268);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Номер Паспорта";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label22);
            this.groupBox10.Location = new System.Drawing.Point(158, 243);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(472, 166);
            this.groupBox10.TabIndex = 20;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Упревление диспетчерами";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(330, 25);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(45, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Пароль";
            // 
            // tabPage6
            // 
            this.tabPage6.AutoScroll = true;
            this.tabPage6.Controls.Add(kvalifLabel);
            this.tabPage6.Controls.Add(this.kvalif_mashinist);
            this.tabPage6.Controls.Add(stajLabel);
            this.tabPage6.Controls.Add(this.staj_mashinist);
            this.tabPage6.Controls.Add(telLabel);
            this.tabPage6.Controls.Add(this.tel_mashinist);
            this.tabPage6.Controls.Add(fioLabel);
            this.tabPage6.Controls.Add(this.fio_mashinist);
            this.tabPage6.Controls.Add(serialLabel);
            this.tabPage6.Controls.Add(serialpasLabel);
            this.tabPage6.Controls.Add(this.serialpas_mashinist);
            this.tabPage6.Controls.Add(this.mashinistDataGridView);
            this.tabPage6.Controls.Add(this.groupBox11);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(789, 423);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Машинисты";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // kvalif_mashinist
            // 
            this.kvalif_mashinist.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mashinistBindingSource, "kvalif", true));
            this.kvalif_mashinist.Location = new System.Drawing.Point(339, 273);
            this.kvalif_mashinist.Name = "kvalif_mashinist";
            this.kvalif_mashinist.Size = new System.Drawing.Size(100, 20);
            this.kvalif_mashinist.TabIndex = 12;
            // 
            // mashinistBindingSource
            // 
            this.mashinistBindingSource.DataMember = "mashinist";
            this.mashinistBindingSource.DataSource = this.metroDataSet;
            // 
            // staj_mashinist
            // 
            this.staj_mashinist.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mashinistBindingSource, "staj", true));
            this.staj_mashinist.Location = new System.Drawing.Point(467, 318);
            this.staj_mashinist.Name = "staj_mashinist";
            this.staj_mashinist.Size = new System.Drawing.Size(100, 20);
            this.staj_mashinist.TabIndex = 10;
            // 
            // tel_mashinist
            // 
            this.tel_mashinist.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mashinistBindingSource, "tel", true));
            this.tel_mashinist.Location = new System.Drawing.Point(467, 273);
            this.tel_mashinist.Name = "tel_mashinist";
            this.tel_mashinist.Size = new System.Drawing.Size(100, 20);
            this.tel_mashinist.TabIndex = 8;
            // 
            // fio_mashinist
            // 
            this.fio_mashinist.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mashinistBindingSource, "fio", true));
            this.fio_mashinist.Location = new System.Drawing.Point(218, 318);
            this.fio_mashinist.Name = "fio_mashinist";
            this.fio_mashinist.Size = new System.Drawing.Size(100, 20);
            this.fio_mashinist.TabIndex = 6;
            // 
            // serialpas_mashinist
            // 
            this.serialpas_mashinist.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mashinistBindingSource, "serialpas", true));
            this.serialpas_mashinist.Location = new System.Drawing.Point(218, 273);
            this.serialpas_mashinist.Name = "serialpas_mashinist";
            this.serialpas_mashinist.Size = new System.Drawing.Size(100, 20);
            this.serialpas_mashinist.TabIndex = 2;
            // 
            // mashinistDataGridView
            // 
            this.mashinistDataGridView.AllowUserToAddRows = false;
            this.mashinistDataGridView.AllowUserToDeleteRows = false;
            this.mashinistDataGridView.AutoGenerateColumns = false;
            this.mashinistDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.mashinistDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mashinistDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40});
            this.mashinistDataGridView.DataSource = this.mashinistBindingSource;
            this.mashinistDataGridView.Location = new System.Drawing.Point(0, 0);
            this.mashinistDataGridView.Name = "mashinistDataGridView";
            this.mashinistDataGridView.ReadOnly = true;
            this.mashinistDataGridView.Size = new System.Drawing.Size(786, 236);
            this.mashinistDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "serialpas";
            this.dataGridViewTextBoxColumn35.HeaderText = "serialpas";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "serial";
            this.dataGridViewTextBoxColumn36.HeaderText = "serial";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "fio";
            this.dataGridViewTextBoxColumn37.HeaderText = "fio";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "tel";
            this.dataGridViewTextBoxColumn38.HeaderText = "tel";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "staj";
            this.dataGridViewTextBoxColumn39.HeaderText = "staj";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.DataPropertyName = "kvalif";
            this.dataGridViewTextBoxColumn40.HeaderText = "kvalif";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.checkBox1);
            this.groupBox11.Controls.Add(this.sostav_mashinist);
            this.groupBox11.Controls.Add(this.del_mashinist);
            this.groupBox11.Controls.Add(this.edit_mashinist);
            this.groupBox11.Controls.Add(this.add_mashinist);
            this.groupBox11.Location = new System.Drawing.Point(194, 242);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(389, 172);
            this.groupBox11.TabIndex = 20;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Управление данными машинистов";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(24, 144);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(134, 17);
            this.checkBox1.TabIndex = 19;
            this.checkBox1.Text = "Отвязать от таблицы";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // sostav_mashinist
            // 
            this.sostav_mashinist.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mashinistBindingSource, "serial", true));
            this.sostav_mashinist.DataSource = this.psostavBindingSource;
            this.sostav_mashinist.DisplayMember = "serial";
            this.sostav_mashinist.FormattingEnabled = true;
            this.sostav_mashinist.Location = new System.Drawing.Point(145, 76);
            this.sostav_mashinist.Name = "sostav_mashinist";
            this.sostav_mashinist.Size = new System.Drawing.Size(100, 21);
            this.sostav_mashinist.TabIndex = 1;
            this.sostav_mashinist.ValueMember = "serial";
            // 
            // del_mashinist
            // 
            this.del_mashinist.Location = new System.Drawing.Point(260, 102);
            this.del_mashinist.Name = "del_mashinist";
            this.del_mashinist.Size = new System.Drawing.Size(113, 36);
            this.del_mashinist.TabIndex = 14;
            this.del_mashinist.Text = "Удалить";
            this.del_mashinist.UseVisualStyleBackColor = true;
            this.del_mashinist.Click += new System.EventHandler(this.del_mashinist_Click);
            // 
            // edit_mashinist
            // 
            this.edit_mashinist.Location = new System.Drawing.Point(141, 103);
            this.edit_mashinist.Name = "edit_mashinist";
            this.edit_mashinist.Size = new System.Drawing.Size(113, 36);
            this.edit_mashinist.TabIndex = 15;
            this.edit_mashinist.Text = "Изменить";
            this.edit_mashinist.UseVisualStyleBackColor = true;
            this.edit_mashinist.Click += new System.EventHandler(this.edit_mashinist_Click);
            // 
            // add_mashinist
            // 
            this.add_mashinist.Enabled = false;
            this.add_mashinist.Location = new System.Drawing.Point(24, 102);
            this.add_mashinist.Name = "add_mashinist";
            this.add_mashinist.Size = new System.Drawing.Size(111, 36);
            this.add_mashinist.TabIndex = 13;
            this.add_mashinist.Text = "Добавить";
            this.add_mashinist.UseVisualStyleBackColor = true;
            this.add_mashinist.Click += new System.EventHandler(this.add_mashinist_Click);
            // 
            // tabPage7
            // 
            this.tabPage7.AutoScroll = true;
            this.tabPage7.Controls.Add(this.v_remont);
            this.tabPage7.Controls.Add(this.v_remontDataGridView);
            this.tabPage7.Controls.Add(this.remontDataGridView);
            this.tabPage7.Controls.Add(this.groupBox5);
            this.tabPage7.Controls.Add(this.groupBox12);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(789, 423);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Ремонт";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // v_remont
            // 
            this.v_remont.AutoSize = true;
            this.v_remont.Location = new System.Drawing.Point(703, 332);
            this.v_remont.Name = "v_remont";
            this.v_remont.Size = new System.Drawing.Size(82, 17);
            this.v_remont.TabIndex = 11;
            this.v_remont.Text = "Служебное";
            this.v_remont.UseVisualStyleBackColor = true;
            this.v_remont.CheckedChanged += new System.EventHandler(this.v_remont_CheckedChanged);
            // 
            // v_remontDataGridView
            // 
            this.v_remontDataGridView.AllowUserToAddRows = false;
            this.v_remontDataGridView.AllowUserToDeleteRows = false;
            this.v_remontDataGridView.AutoGenerateColumns = false;
            this.v_remontDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.v_remontDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.v_remontDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn72,
            this.dataGridViewTextBoxColumn73,
            this.dataGridViewTextBoxColumn74,
            this.dataGridViewTextBoxColumn75,
            this.dataGridViewTextBoxColumn76});
            this.v_remontDataGridView.DataSource = this.v_remontBindingSource;
            this.v_remontDataGridView.Location = new System.Drawing.Point(0, 0);
            this.v_remontDataGridView.Name = "v_remontDataGridView";
            this.v_remontDataGridView.ReadOnly = true;
            this.v_remontDataGridView.Size = new System.Drawing.Size(786, 326);
            this.v_remontDataGridView.TabIndex = 10;
            // 
            // dataGridViewTextBoxColumn72
            // 
            this.dataGridViewTextBoxColumn72.DataPropertyName = "Тупик";
            this.dataGridViewTextBoxColumn72.HeaderText = "Тупик";
            this.dataGridViewTextBoxColumn72.Name = "dataGridViewTextBoxColumn72";
            this.dataGridViewTextBoxColumn72.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn73
            // 
            this.dataGridViewTextBoxColumn73.DataPropertyName = "Состав";
            this.dataGridViewTextBoxColumn73.HeaderText = "Состав";
            this.dataGridViewTextBoxColumn73.Name = "dataGridViewTextBoxColumn73";
            this.dataGridViewTextBoxColumn73.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn74
            // 
            this.dataGridViewTextBoxColumn74.DataPropertyName = "Неисправность";
            this.dataGridViewTextBoxColumn74.HeaderText = "Неисправность";
            this.dataGridViewTextBoxColumn74.Name = "dataGridViewTextBoxColumn74";
            this.dataGridViewTextBoxColumn74.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn75
            // 
            this.dataGridViewTextBoxColumn75.DataPropertyName = "Время начала";
            this.dataGridViewTextBoxColumn75.HeaderText = "Время начала";
            this.dataGridViewTextBoxColumn75.Name = "dataGridViewTextBoxColumn75";
            this.dataGridViewTextBoxColumn75.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn76
            // 
            this.dataGridViewTextBoxColumn76.DataPropertyName = "Предположительное окончание";
            this.dataGridViewTextBoxColumn76.HeaderText = "Предположительное окончание";
            this.dataGridViewTextBoxColumn76.Name = "dataGridViewTextBoxColumn76";
            this.dataGridViewTextBoxColumn76.ReadOnly = true;
            // 
            // v_remontBindingSource
            // 
            this.v_remontBindingSource.DataMember = "v_remont";
            this.v_remontBindingSource.DataSource = this.metroDataSet;
            // 
            // remontDataGridView
            // 
            this.remontDataGridView.AllowUserToAddRows = false;
            this.remontDataGridView.AllowUserToDeleteRows = false;
            this.remontDataGridView.AutoGenerateColumns = false;
            this.remontDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.remontDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.remontDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45});
            this.remontDataGridView.DataSource = this.remontBindingSource;
            this.remontDataGridView.Location = new System.Drawing.Point(0, 0);
            this.remontDataGridView.Name = "remontDataGridView";
            this.remontDataGridView.ReadOnly = true;
            this.remontDataGridView.Size = new System.Drawing.Size(786, 326);
            this.remontDataGridView.TabIndex = 0;
            this.remontDataGridView.Visible = false;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.DataPropertyName = "typik";
            this.dataGridViewTextBoxColumn42.HeaderText = "typik";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.DataPropertyName = "serial";
            this.dataGridViewTextBoxColumn43.HeaderText = "serial";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.DataPropertyName = "start";
            this.dataGridViewTextBoxColumn44.HeaderText = "start";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.DataPropertyName = "kod";
            this.dataGridViewTextBoxColumn45.HeaderText = "kod";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.ReadOnly = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.serial_remont);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.namevetka_remont);
            this.groupBox5.Controls.Add(this.del_remont);
            this.groupBox5.Location = new System.Drawing.Point(6, 332);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(324, 83);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Возвращение в депо";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(105, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "Ветка";
            // 
            // serial_remont
            // 
            this.serial_remont.DataSource = this.remontBindingSource;
            this.serial_remont.DisplayMember = "serial";
            this.serial_remont.FormattingEnabled = true;
            this.serial_remont.Location = new System.Drawing.Point(6, 39);
            this.serial_remont.Name = "serial_remont";
            this.serial_remont.Size = new System.Drawing.Size(93, 21);
            this.serial_remont.TabIndex = 1;
            this.serial_remont.ValueMember = "serial";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(93, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "Серийный номер";
            // 
            // namevetka_remont
            // 
            this.namevetka_remont.DataSource = this.vetkaBindingSource;
            this.namevetka_remont.DisplayMember = "name";
            this.namevetka_remont.FormattingEnabled = true;
            this.namevetka_remont.Location = new System.Drawing.Point(105, 39);
            this.namevetka_remont.Name = "namevetka_remont";
            this.namevetka_remont.Size = new System.Drawing.Size(91, 21);
            this.namevetka_remont.TabIndex = 2;
            this.namevetka_remont.ValueMember = "name";
            // 
            // del_remont
            // 
            this.del_remont.Location = new System.Drawing.Point(202, 37);
            this.del_remont.Name = "del_remont";
            this.del_remont.Size = new System.Drawing.Size(107, 23);
            this.del_remont.TabIndex = 3;
            this.del_remont.Text = "Вернуть в депо";
            this.del_remont.UseVisualStyleBackColor = true;
            this.del_remont.Click += new System.EventHandler(this.del_remont_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.button_del_typik);
            this.groupBox12.Controls.Add(this.button_add_typik);
            this.groupBox12.Controls.Add(this.label20);
            this.groupBox12.Controls.Add(this.add_typik);
            this.groupBox12.Location = new System.Drawing.Point(336, 334);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(292, 83);
            this.groupBox12.TabIndex = 10;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Управление тупиком";
            // 
            // button_del_typik
            // 
            this.button_del_typik.Location = new System.Drawing.Point(206, 23);
            this.button_del_typik.Name = "button_del_typik";
            this.button_del_typik.Size = new System.Drawing.Size(75, 40);
            this.button_del_typik.TabIndex = 11;
            this.button_del_typik.Text = "Удалить";
            this.button_del_typik.UseVisualStyleBackColor = true;
            this.button_del_typik.Click += new System.EventHandler(this.button_del_typik_Click);
            // 
            // button_add_typik
            // 
            this.button_add_typik.Location = new System.Drawing.Point(125, 23);
            this.button_add_typik.Name = "button_add_typik";
            this.button_add_typik.Size = new System.Drawing.Size(75, 40);
            this.button_add_typik.TabIndex = 9;
            this.button_add_typik.Text = "Добавить";
            this.button_add_typik.UseVisualStyleBackColor = true;
            this.button_add_typik.Click += new System.EventHandler(this.button_add_typik_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 23);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 13);
            this.label20.TabIndex = 8;
            this.label20.Text = "Номер тупика";
            // 
            // add_typik
            // 
            this.add_typik.Location = new System.Drawing.Point(9, 40);
            this.add_typik.Name = "add_typik";
            this.add_typik.Size = new System.Drawing.Size(110, 20);
            this.add_typik.TabIndex = 7;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.spravkaDataGridView);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(789, 423);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Справка";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // spravkaDataGridView
            // 
            this.spravkaDataGridView.AllowUserToAddRows = false;
            this.spravkaDataGridView.AllowUserToDeleteRows = false;
            this.spravkaDataGridView.AutoGenerateColumns = false;
            this.spravkaDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.spravkaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.spravkaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51});
            this.spravkaDataGridView.DataSource = this.spravkaBindingSource;
            this.spravkaDataGridView.Location = new System.Drawing.Point(0, 0);
            this.spravkaDataGridView.Name = "spravkaDataGridView";
            this.spravkaDataGridView.ReadOnly = true;
            this.spravkaDataGridView.Size = new System.Drawing.Size(789, 265);
            this.spravkaDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.DataPropertyName = "kod";
            this.dataGridViewTextBoxColumn47.HeaderText = "kod";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn48.HeaderText = "name";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.DataPropertyName = "krit";
            this.dataGridViewTextBoxColumn49.HeaderText = "krit";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.DataPropertyName = "srok_dos_det";
            this.dataGridViewTextBoxColumn50.HeaderText = "srok_dos_det";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.DataPropertyName = "vremya_remonta";
            this.dataGridViewTextBoxColumn51.HeaderText = "vremya_remonta";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            // 
            // spravkaBindingSource
            // 
            this.spravkaBindingSource.DataMember = "spravka";
            this.spravkaBindingSource.DataSource = this.metroDataSet;
            // 
            // tabPage9
            // 
            this.tabPage9.AutoScroll = true;
            this.tabPage9.Controls.Add(this.button_del_stanciya);
            this.tabPage9.Controls.Add(this.button_edit_stanciya);
            this.tabPage9.Controls.Add(this.stat_stanciya);
            this.tabPage9.Controls.Add(this.finish_time_stanciya);
            this.tabPage9.Controls.Add(this.start_time_stanciya);
            this.tabPage9.Controls.Add(this.paspotok_stanciya);
            this.tabPage9.Controls.Add(this.name_stanciya);
            this.tabPage9.Controls.Add(this.kod_stanciya);
            this.tabPage9.Controls.Add(this.stanciyaDataGridView);
            this.tabPage9.Controls.Add(this.groupBox_stanciya);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(789, 423);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "Станции";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // button_del_stanciya
            // 
            this.button_del_stanciya.Location = new System.Drawing.Point(539, 356);
            this.button_del_stanciya.Name = "button_del_stanciya";
            this.button_del_stanciya.Size = new System.Drawing.Size(77, 38);
            this.button_del_stanciya.TabIndex = 15;
            this.button_del_stanciya.Text = "Удалить";
            this.button_del_stanciya.UseVisualStyleBackColor = true;
            this.button_del_stanciya.Click += new System.EventHandler(this.button_del_stanciya_Click);
            // 
            // button_edit_stanciya
            // 
            this.button_edit_stanciya.Location = new System.Drawing.Point(450, 356);
            this.button_edit_stanciya.Name = "button_edit_stanciya";
            this.button_edit_stanciya.Size = new System.Drawing.Size(83, 38);
            this.button_edit_stanciya.TabIndex = 14;
            this.button_edit_stanciya.Text = "Изменить";
            this.button_edit_stanciya.UseVisualStyleBackColor = true;
            this.button_edit_stanciya.Click += new System.EventHandler(this.button_edit_stanciya_Click);
            // 
            // stat_stanciya
            // 
            this.stat_stanciya.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stanciyaBindingSource, "stat", true));
            this.stat_stanciya.Location = new System.Drawing.Point(265, 330);
            this.stat_stanciya.Name = "stat_stanciya";
            this.stat_stanciya.Size = new System.Drawing.Size(139, 20);
            this.stat_stanciya.TabIndex = 12;
            // 
            // stanciyaBindingSource
            // 
            this.stanciyaBindingSource.DataMember = "stanciya";
            this.stanciyaBindingSource.DataSource = this.metroDataSet;
            // 
            // finish_time_stanciya
            // 
            this.finish_time_stanciya.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stanciyaBindingSource, "finish_time", true));
            this.finish_time_stanciya.Location = new System.Drawing.Point(265, 373);
            this.finish_time_stanciya.Name = "finish_time_stanciya";
            this.finish_time_stanciya.Size = new System.Drawing.Size(100, 20);
            this.finish_time_stanciya.TabIndex = 11;
            // 
            // start_time_stanciya
            // 
            this.start_time_stanciya.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stanciyaBindingSource, "start_time", true));
            this.start_time_stanciya.Location = new System.Drawing.Point(159, 373);
            this.start_time_stanciya.Name = "start_time_stanciya";
            this.start_time_stanciya.Size = new System.Drawing.Size(100, 20);
            this.start_time_stanciya.TabIndex = 10;
            // 
            // paspotok_stanciya
            // 
            this.paspotok_stanciya.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stanciyaBindingSource, "paspotok", true));
            this.paspotok_stanciya.Location = new System.Drawing.Point(159, 330);
            this.paspotok_stanciya.Name = "paspotok_stanciya";
            this.paspotok_stanciya.Size = new System.Drawing.Size(100, 20);
            this.paspotok_stanciya.TabIndex = 6;
            // 
            // name_stanciya
            // 
            this.name_stanciya.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stanciyaBindingSource, "name", true));
            this.name_stanciya.Location = new System.Drawing.Point(265, 291);
            this.name_stanciya.Name = "name_stanciya";
            this.name_stanciya.Size = new System.Drawing.Size(139, 20);
            this.name_stanciya.TabIndex = 4;
            // 
            // kod_stanciya
            // 
            this.kod_stanciya.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stanciyaBindingSource, "kod", true));
            this.kod_stanciya.Location = new System.Drawing.Point(159, 291);
            this.kod_stanciya.Name = "kod_stanciya";
            this.kod_stanciya.Size = new System.Drawing.Size(100, 20);
            this.kod_stanciya.TabIndex = 2;
            // 
            // stanciyaDataGridView
            // 
            this.stanciyaDataGridView.AllowUserToAddRows = false;
            this.stanciyaDataGridView.AllowUserToDeleteRows = false;
            this.stanciyaDataGridView.AutoGenerateColumns = false;
            this.stanciyaDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.stanciyaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.stanciyaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewTextBoxColumn61});
            this.stanciyaDataGridView.DataSource = this.stanciyaBindingSource;
            this.stanciyaDataGridView.Location = new System.Drawing.Point(3, 0);
            this.stanciyaDataGridView.Name = "stanciyaDataGridView";
            this.stanciyaDataGridView.ReadOnly = true;
            this.stanciyaDataGridView.Size = new System.Drawing.Size(783, 249);
            this.stanciyaDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.DataPropertyName = "kod";
            this.dataGridViewTextBoxColumn52.HeaderText = "kod";
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn53.HeaderText = "name";
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.DataPropertyName = "paspotok";
            this.dataGridViewTextBoxColumn54.HeaderText = "paspotok";
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.DataPropertyName = "kod_vpered";
            this.dataGridViewTextBoxColumn55.HeaderText = "kod_vpered";
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.DataPropertyName = "rast_vpered";
            this.dataGridViewTextBoxColumn56.HeaderText = "rast_vpered";
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.DataPropertyName = "kod_nazad";
            this.dataGridViewTextBoxColumn57.HeaderText = "kod_nazad";
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.DataPropertyName = "rast_nazad";
            this.dataGridViewTextBoxColumn58.HeaderText = "rast_nazad";
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.DataPropertyName = "stat";
            this.dataGridViewTextBoxColumn59.HeaderText = "stat";
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.DataPropertyName = "start_time";
            this.dataGridViewTextBoxColumn60.HeaderText = "start_time";
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn61
            // 
            this.dataGridViewTextBoxColumn61.DataPropertyName = "finish_time";
            this.dataGridViewTextBoxColumn61.HeaderText = "finish_time";
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            this.dataGridViewTextBoxColumn61.ReadOnly = true;
            // 
            // groupBox_stanciya
            // 
            this.groupBox_stanciya.Controls.Add(this.kod_nazad_stanciya);
            this.groupBox_stanciya.Controls.Add(this.kod_vpered_stanciya);
            this.groupBox_stanciya.Controls.Add(this.label32);
            this.groupBox_stanciya.Controls.Add(this.label31);
            this.groupBox_stanciya.Controls.Add(this.label30);
            this.groupBox_stanciya.Controls.Add(this.label29);
            this.groupBox_stanciya.Controls.Add(this.label28);
            this.groupBox_stanciya.Controls.Add(this.label27);
            this.groupBox_stanciya.Controls.Add(this.label26);
            this.groupBox_stanciya.Controls.Add(this.label25);
            this.groupBox_stanciya.Controls.Add(this.label24);
            this.groupBox_stanciya.Controls.Add(this.label23);
            this.groupBox_stanciya.Controls.Add(this.button_add_stanciya);
            this.groupBox_stanciya.Controls.Add(this.checkBox2);
            this.groupBox_stanciya.Controls.Add(this.rast_vpered_stanciya);
            this.groupBox_stanciya.Controls.Add(this.rast_nazad_stanciya);
            this.groupBox_stanciya.Location = new System.Drawing.Point(148, 252);
            this.groupBox_stanciya.Name = "groupBox_stanciya";
            this.groupBox_stanciya.Size = new System.Drawing.Size(488, 168);
            this.groupBox_stanciya.TabIndex = 24;
            this.groupBox_stanciya.TabStop = false;
            this.groupBox_stanciya.Text = "Управление станциями";
            // 
            // kod_nazad_stanciya
            // 
            this.kod_nazad_stanciya.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stanciyaBindingSource, "kod_nazad", true));
            this.kod_nazad_stanciya.Location = new System.Drawing.Point(376, 39);
            this.kod_nazad_stanciya.Name = "kod_nazad_stanciya";
            this.kod_nazad_stanciya.Size = new System.Drawing.Size(100, 20);
            this.kod_nazad_stanciya.TabIndex = 34;
            // 
            // kod_vpered_stanciya
            // 
            this.kod_vpered_stanciya.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stanciyaBindingSource, "kod_vpered", true));
            this.kod_vpered_stanciya.Location = new System.Drawing.Point(270, 39);
            this.kod_vpered_stanciya.Name = "kod_vpered_stanciya";
            this.kod_vpered_stanciya.Size = new System.Drawing.Size(100, 20);
            this.kod_vpered_stanciya.TabIndex = 33;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(117, 62);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 13);
            this.label32.TabIndex = 32;
            this.label32.Text = "Статус";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(114, 102);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(78, 13);
            this.label31.TabIndex = 31;
            this.label31.Text = "Конец работы";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(11, 102);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(84, 13);
            this.label30.TabIndex = 30;
            this.label30.Text = "Начало работы";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(373, 62);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(94, 13);
            this.label29.TabIndex = 29;
            this.label29.Text = "Растояние назад";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(267, 62);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(100, 13);
            this.label28.TabIndex = 28;
            this.label28.Text = "Растояние вперед";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(8, 62);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 13);
            this.label27.TabIndex = 27;
            this.label27.Text = "Пассажиропоток";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(373, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(97, 13);
            this.label26.TabIndex = 26;
            this.label26.Text = "Код пред станции";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(267, 23);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(97, 13);
            this.label25.TabIndex = 25;
            this.label25.Text = "Код след станции";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(117, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(57, 13);
            this.label24.TabIndex = 24;
            this.label24.Text = "Название";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(8, 23);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(70, 13);
            this.label23.TabIndex = 23;
            this.label23.Text = "Код станции";
            // 
            // button_add_stanciya
            // 
            this.button_add_stanciya.Enabled = false;
            this.button_add_stanciya.Location = new System.Drawing.Point(223, 105);
            this.button_add_stanciya.Name = "button_add_stanciya";
            this.button_add_stanciya.Size = new System.Drawing.Size(73, 37);
            this.button_add_stanciya.TabIndex = 13;
            this.button_add_stanciya.Text = "Добавить";
            this.button_add_stanciya.UseVisualStyleBackColor = true;
            this.button_add_stanciya.Click += new System.EventHandler(this.button_add_stanciya_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(223, 147);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(127, 17);
            this.checkBox2.TabIndex = 0;
            this.checkBox2.Text = "Отвязать от формы";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // rast_vpered_stanciya
            // 
            this.rast_vpered_stanciya.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stanciyaBindingSource, "rast_vpered", true));
            this.rast_vpered_stanciya.Location = new System.Drawing.Point(270, 78);
            this.rast_vpered_stanciya.Name = "rast_vpered_stanciya";
            this.rast_vpered_stanciya.Size = new System.Drawing.Size(100, 20);
            this.rast_vpered_stanciya.TabIndex = 17;
            // 
            // rast_nazad_stanciya
            // 
            this.rast_nazad_stanciya.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.stanciyaBindingSource, "rast_nazad", true));
            this.rast_nazad_stanciya.Location = new System.Drawing.Point(376, 78);
            this.rast_nazad_stanciya.Name = "rast_nazad_stanciya";
            this.rast_nazad_stanciya.Size = new System.Drawing.Size(100, 20);
            this.rast_nazad_stanciya.TabIndex = 19;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.jurnalDataGridView);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(789, 423);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "Журнал ремонта";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // jurnalDataGridView
            // 
            this.jurnalDataGridView.AllowUserToAddRows = false;
            this.jurnalDataGridView.AllowUserToDeleteRows = false;
            this.jurnalDataGridView.AutoGenerateColumns = false;
            this.jurnalDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.jurnalDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.jurnalDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64,
            this.dataGridViewTextBoxColumn65,
            this.dataGridViewTextBoxColumn66});
            this.jurnalDataGridView.DataSource = this.jurnalBindingSource;
            this.jurnalDataGridView.Location = new System.Drawing.Point(0, 0);
            this.jurnalDataGridView.Name = "jurnalDataGridView";
            this.jurnalDataGridView.ReadOnly = true;
            this.jurnalDataGridView.Size = new System.Drawing.Size(789, 423);
            this.jurnalDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn62
            // 
            this.dataGridViewTextBoxColumn62.DataPropertyName = "start";
            this.dataGridViewTextBoxColumn62.HeaderText = "start";
            this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
            this.dataGridViewTextBoxColumn62.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn63
            // 
            this.dataGridViewTextBoxColumn63.DataPropertyName = "serialpas";
            this.dataGridViewTextBoxColumn63.HeaderText = "serialpas";
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            this.dataGridViewTextBoxColumn63.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn64
            // 
            this.dataGridViewTextBoxColumn64.DataPropertyName = "serial";
            this.dataGridViewTextBoxColumn64.HeaderText = "serial";
            this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
            this.dataGridViewTextBoxColumn64.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn65
            // 
            this.dataGridViewTextBoxColumn65.DataPropertyName = "typik";
            this.dataGridViewTextBoxColumn65.HeaderText = "typik";
            this.dataGridViewTextBoxColumn65.Name = "dataGridViewTextBoxColumn65";
            this.dataGridViewTextBoxColumn65.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn66
            // 
            this.dataGridViewTextBoxColumn66.DataPropertyName = "zametka";
            this.dataGridViewTextBoxColumn66.HeaderText = "zametka";
            this.dataGridViewTextBoxColumn66.Name = "dataGridViewTextBoxColumn66";
            this.dataGridViewTextBoxColumn66.ReadOnly = true;
            // 
            // jurnalBindingSource
            // 
            this.jurnalBindingSource.DataMember = "jurnal";
            this.jurnalBindingSource.DataSource = this.metroDataSet;
            // 
            // raspisanieTableAdapter
            // 
            this.raspisanieTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.dispetcherTableAdapter = null;
            this.tableAdapterManager.jurnalTableAdapter = null;
            this.tableAdapterManager.mashinistTableAdapter = null;
            this.tableAdapterManager.psostavTableAdapter = null;
            this.tableAdapterManager.raspisanieTableAdapter = this.raspisanieTableAdapter;
            this.tableAdapterManager.remontTableAdapter = null;
            this.tableAdapterManager.spravkaTableAdapter = null;
            this.tableAdapterManager.stanciyaTableAdapter = null;
            this.tableAdapterManager.stanitsaTableAdapter = null;
            this.tableAdapterManager.typsostavTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Metro.MetroDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.vetkaTableAdapter = null;
            // 
            // psostavTableAdapter
            // 
            this.psostavTableAdapter.ClearBeforeFill = true;
            // 
            // stanciyaTableAdapter
            // 
            this.stanciyaTableAdapter.ClearBeforeFill = true;
            // 
            // vetkaTableAdapter
            // 
            this.vetkaTableAdapter.ClearBeforeFill = true;
            // 
            // fKvetkaraspisanieBindingSource
            // 
            this.fKvetkaraspisanieBindingSource.DataMember = "FK_vetka_raspisanie";
            this.fKvetkaraspisanieBindingSource.DataSource = this.vetkaBindingSource;
            // 
            // fKvetkaraspisanieBindingSource1
            // 
            this.fKvetkaraspisanieBindingSource1.DataMember = "FK_vetka_raspisanie";
            this.fKvetkaraspisanieBindingSource1.DataSource = this.vetkaBindingSource;
            // 
            // typsostavTableAdapter
            // 
            this.typsostavTableAdapter.ClearBeforeFill = true;
            // 
            // dispetcherTableAdapter
            // 
            this.dispetcherTableAdapter.ClearBeforeFill = true;
            // 
            // mashinistTableAdapter
            // 
            this.mashinistTableAdapter.ClearBeforeFill = true;
            // 
            // remontTableAdapter
            // 
            this.remontTableAdapter.ClearBeforeFill = true;
            // 
            // spravkaTableAdapter
            // 
            this.spravkaTableAdapter.ClearBeforeFill = true;
            // 
            // jurnalTableAdapter
            // 
            this.jurnalTableAdapter.ClearBeforeFill = true;
            // 
            // v_raspisanieTableAdapter
            // 
            this.v_raspisanieTableAdapter.ClearBeforeFill = true;
            // 
            // v_remontTableAdapter
            // 
            this.v_remontTableAdapter.ClearBeforeFill = true;
            // 
            // v_psostavBindingSource
            // 
            this.v_psostavBindingSource.DataSource = this.metroDataSet;
            this.v_psostavBindingSource.Position = 0;
            // 
            // v_psostavBindingSource1
            // 
            this.v_psostavBindingSource1.DataSource = this.metroDataSet;
            this.v_psostavBindingSource1.Position = 0;
            // 
            // v_psostavBindingSource2
            // 
            this.v_psostavBindingSource2.DataSource = this.metroDataSet;
            this.v_psostavBindingSource2.Position = 0;
            // 
            // v_psostavBindingSource3
            // 
            this.v_psostavBindingSource3.DataSource = this.metroDataSet;
            this.v_psostavBindingSource3.Position = 0;
            // 
            // fKtypkodpsostavBindingSource
            // 
            this.fKtypkodpsostavBindingSource.DataMember = "FK_typkod_psostav";
            this.fKtypkodpsostavBindingSource.DataSource = this.typsostavBindingSource;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Служба движения Метрополитена";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_raspisanieDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_raspisanieBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.raspisanieDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.raspisanieBindingSource)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vetkaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psostavBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.typsostavBindingSource)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.psostavDataGridView)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.remontBindingSource)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vetkaDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.typsostavDataGridView)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dispetcherDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dispetcherBindingSource)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mashinistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mashinistDataGridView)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.v_remontDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_remontBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.remontDataGridView)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spravkaDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spravkaBindingSource)).EndInit();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stanciyaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stanciyaDataGridView)).EndInit();
            this.groupBox_stanciya.ResumeLayout(false);
            this.groupBox_stanciya.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.jurnalDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jurnalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKvetkaraspisanieBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKvetkaraspisanieBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_raspisanieBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_psostavBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_psostavBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_psostavBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_psostavBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKtypkodpsostavBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private MetroDataSet metroDataSet;
        private System.Windows.Forms.BindingSource raspisanieBindingSource;
        private MetroDataSetTableAdapters.raspisanieTableAdapter raspisanieTableAdapter;
        private MetroDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Button optimal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox typ;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.BindingSource psostavBindingSource;
        private MetroDataSetTableAdapters.psostavTableAdapter psostavTableAdapter;
        private System.Windows.Forms.DateTimePicker start_time;
        private System.Windows.Forms.DataGridView psostavDataGridView;
        private System.Windows.Forms.DateTimePicker finish_time;
        private System.Windows.Forms.ComboBox vetka;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.BindingSource stanciyaBindingSource;
        private MetroDataSetTableAdapters.stanciyaTableAdapter stanciyaTableAdapter;
        private System.Windows.Forms.BindingSource vetkaBindingSource;
        private MetroDataSetTableAdapters.vetkaTableAdapter vetkaTableAdapter;
        private System.Windows.Forms.DataGridView vetkaDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.NumericUpDown kol;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.BindingSource fKvetkaraspisanieBindingSource;
        private System.Windows.Forms.BindingSource fKvetkaraspisanieBindingSource1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.BindingSource typsostavBindingSource;
        private MetroDataSetTableAdapters.typsostavTableAdapter typsostavTableAdapter;
        private System.Windows.Forms.DataGridView typsostavDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.BindingSource v_raspisanieBindingSource;
        private System.Windows.Forms.DataGridView raspisanieDataGridView;
        private System.Windows.Forms.Button del_rasp;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.BindingSource dispetcherBindingSource;
        private MetroDataSetTableAdapters.dispetcherTableAdapter dispetcherTableAdapter;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.BindingSource mashinistBindingSource;
        private MetroDataSetTableAdapters.mashinistTableAdapter mashinistTableAdapter;
        private System.Windows.Forms.DataGridView mashinistDataGridView;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.BindingSource remontBindingSource;
        private MetroDataSetTableAdapters.remontTableAdapter remontTableAdapter;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.BindingSource spravkaBindingSource;
        private MetroDataSetTableAdapters.spravkaTableAdapter spravkaTableAdapter;
        private System.Windows.Forms.DataGridView spravkaDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridView stanciyaDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.BindingSource jurnalBindingSource;
        private MetroDataSetTableAdapters.jurnalTableAdapter jurnalTableAdapter;
        private System.Windows.Forms.DataGridView jurnalDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn65;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn66;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker n_time;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button add_raspis;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox add_vetka;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox serial;
        private System.Windows.Forms.Button add_dispetcher;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox serialpas;
        private System.Windows.Forms.TextBox doljnost;
        private System.Windows.Forms.TextBox tel;
        private System.Windows.Forms.TextBox fio;
        private System.Windows.Forms.TextBox pas;
        private System.Windows.Forms.Button del_dispetcher;
        private System.Windows.Forms.Button edit_dispetcher;
        private System.Windows.Forms.TextBox kvalif_mashinist;
        private System.Windows.Forms.TextBox staj_mashinist;
        private System.Windows.Forms.TextBox tel_mashinist;
        private System.Windows.Forms.TextBox fio_mashinist;
        private System.Windows.Forms.TextBox serialpas_mashinist;
        private System.Windows.Forms.Button edit_mashinist;
        private System.Windows.Forms.Button del_mashinist;
        private System.Windows.Forms.Button add_mashinist;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox_dispetcher;
        private System.Windows.Forms.CheckBox checkBox_psostav;
        private System.Windows.Forms.Button edit_psostav;
        private System.Windows.Forms.Button del_psostav;
        private System.Windows.Forms.Button add_psostav;
        private System.Windows.Forms.TextBox iznos_tk_psostav;
        private System.Windows.Forms.TextBox iznos_motor_psostav;
        private System.Windows.Forms.TextBox probeg_psostav;
        private System.Windows.Forms.TextBox finish_status_vetka;
        private System.Windows.Forms.TextBox start_status_vetka;
        private System.Windows.Forms.TextBox depo_status_stanciya;
        private System.Windows.Forms.TextBox name_vetka;
        private System.Windows.Forms.Button button_del_vetka;
        private System.Windows.Forms.Button button_add_vetka;
        private System.Windows.Forms.Button button_del_stanciya;
        private System.Windows.Forms.Button button_edit_stanciya;
        private System.Windows.Forms.Button button_add_stanciya;
        private System.Windows.Forms.TextBox stat_stanciya;
        private System.Windows.Forms.TextBox finish_time_stanciya;
        private System.Windows.Forms.TextBox start_time_stanciya;
        private System.Windows.Forms.TextBox paspotok_stanciya;
        private System.Windows.Forms.TextBox name_stanciya;
        private System.Windows.Forms.TextBox kod_stanciya;
        private System.Windows.Forms.TextBox rast_nazad_stanciya;
        private System.Windows.Forms.TextBox rast_vpered_stanciya;
        private System.Windows.Forms.Button button_add_remont;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button del_remont;
        private System.Windows.Forms.ComboBox namevetka_remont;
        private System.Windows.Forms.ComboBox serial_remont;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox typik_psostav;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox zametka_psostav;
        private System.Windows.Forms.DataGridView dispetcherDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.GroupBox groupBox_stanciya;
        private System.Windows.Forms.Button button_del_typsostav;
        private System.Windows.Forms.Button button_edit_typsostav;
        private System.Windows.Forms.Button button_add_typsostav;
        private System.Windows.Forms.TextBox speed_typsostav;
        private System.Windows.Forms.TextBox koleya_typsostav;
        private System.Windows.Forms.TextBox naznachenie_typsostav;
        private System.Windows.Forms.TextBox visota_typsostav;
        private System.Windows.Forms.TextBox shirina_typsostav;
        private System.Windows.Forms.TextBox dlina_typsostav;
        private System.Windows.Forms.TextBox kolmest_typsostav;
        private System.Windows.Forms.TextBox name_typsostav;
        private System.Windows.Forms.TextBox typkod_typsostav;
        private System.Windows.Forms.CheckBox checkBox_typsostav;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TextBox start_stanciya_vetka;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox del_name_vetka;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox smena_raspisanie;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.BindingSource v_raspisanieBindingSource1;
        private MetroDataSetTableAdapters.v_raspisanieTableAdapter v_raspisanieTableAdapter;
        private System.Windows.Forms.DataGridView v_raspisanieDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn67;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn68;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn69;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn70;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn71;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button button_add_typik;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox add_typik;
        private System.Windows.Forms.Button button_del_typik;
        private System.Windows.Forms.BindingSource v_remontBindingSource;
        private MetroDataSetTableAdapters.v_remontTableAdapter v_remontTableAdapter;
        private System.Windows.Forms.CheckBox v_remont;
        private System.Windows.Forms.ComboBox sostav_mashinist;
        private System.Windows.Forms.TextBox serial_psostav;
        private System.Windows.Forms.BindingSource v_psostavBindingSource;
        private System.Windows.Forms.BindingSource v_psostavBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.BindingSource v_psostavBindingSource2;
        private System.Windows.Forms.BindingSource v_psostavBindingSource3;
        private System.Windows.Forms.ComboBox serial_add_remont;
        private System.Windows.Forms.DataGridView v_remontDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn72;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn73;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn74;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn75;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn76;
        private System.Windows.Forms.DataGridView remontDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.ComboBox typkod_psostav;
        private System.Windows.Forms.BindingSource fKtypkodpsostavBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn start_vremya;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox kod_nazad_stanciya;
        private System.Windows.Forms.TextBox kod_vpered_stanciya;
        private System.Windows.Forms.ComboBox stat_sostav;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox name_vetka2;
    }
}

